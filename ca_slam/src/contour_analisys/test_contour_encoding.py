#!/usr/bin/env python

import unittest
import contour_encoding
import numpy as np
from sensor_msgs.msg import PointCloud
from geometry_msgs.msg import Point32

class TestContourEncoding(unittest.TestCase):
        
    def test_convertLaserScanToPolarCode(self):
        scan = [37, 25, 17, 24, 17, 24, 22, 31]
        polarCode = [37, 17.67 + 17.67j, 17j, -16.97 + 16.97j, -17, -16.97 - 16.97j, -22j, 21.92 - 21.92j]

        result = contour_encoding.convertLaserScanToPolarCode(scan)
        np.testing.assert_almost_equal(result, polarCode, 2)

    def test_convertPolarCodeToDifferenceCode(self):
        polarCode = [37, 17.67 + 17.67j, 17j, -16.97 + 16.97j, -17, -16.97 - 16.97j, -22j, 21.92 - 21.92j]
        differenceCode = [-19.33 + 17.67j, -17.67 - 0.67j, -16.97 - 0.03j, -0.03 - 16.97j, 0.03 - 16.97j, 16.97 - 5.03j, 21.92 + 0.08j]

        result = contour_encoding.convertPolarCodeToDifferenceCode(polarCode)
        np.testing.assert_almost_equal(result, differenceCode, 2)

    def test_equivalenceOfContourCode(self):
        contour = [2, 2 - 2j, -1j, -2 - 2j, -2 + 2j, 1j, -1 + 1j, 1 + 1j]
        equivalenceContour = [1.27, 1.11 - 0.39j, 0.9 - 0.9j, 0.71 - 0.98j, -0.39 - 1.11j, -0.9 -0.9j, -0.9 - 0.51j, -0.9 + 0.9j, -0.9 + 0.9j, -0.2 + 1.2j, -0.71 + 0.9j, 0.91 + 0.89j]

        result = contour_encoding.equivalenceOfContourCode(contour, 1.2761423749)
        np.testing.assert_almost_equal(result, equivalenceContour, 2)