#!/usr/bin/env python

"""
Module describing kinematic models of robots
"""

import math
import numpy as np
from abc import ABCMeta, abstractmethod
from ekf.ekf_system_model import EkfSystemModel, EkfControlAction

class RobotModel(EkfSystemModel, EkfControlAction):
    """
    Abstract class describing of the model robot
    """
    __metaclass__  = ABCMeta

    def __init__(self):
        super(RobotModel, self).__init__()
        self._position = np.array([0, 0, 0], dtype="float32")
        self.__lidarDistance = 0.08
        self.__cameraDistance = -0.07
        self._sensorData = None

    def getPosition(self):
        """
        Returns position
        Returns:
            ([float, float, float]) - x, y, theta coordinates
        """
        return self._position

    def getLidarDistance(self):
        """
        Returns distance beetwen the lidar position and the center of the robot
        Returns:
            (float) - distance beetwen the lidar position and the center of the robot
        """
        return self.__lidarDistance

    def getCameraDistance(self):
        """
        Returns distance beetwen the camera position and the center of the robot
        Returns:
            (float) - distance beetwen the camera position and the center of the robot
        """
        return self.__cameraDistance
    
    def getCameraPosition(self):
        """
        Return camera position
        Returns:
            ([float, float, float]) - x, y, z camera coordinates
        """
        return np.array([self._position[0] - self.__cameraDistance * math.cos(self._position[2]), self._position[1] + self.__cameraDistance * math.sin(self._position[2]), 0.0])

    def getSensorData(self):
        """
        Returns sensor data
        Returns
            (SensorData) - sensor data
        """
        return self._sensorData

    @abstractmethod
    def update(self, sensorData):
        """
        Update position, stateTransitionMatrix, controlMatrix
        Args:
            sensorData (sendosrData) - sendor data
        """
        pass