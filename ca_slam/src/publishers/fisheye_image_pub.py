#!/usr/bin/env python

"""
Module containing fisheye image publisher
"""

import rospy
import numpy as np
import image_processing
from sensor_msgs.msg import Image

class FisheyeImagePublisher():
    """
    """
    def __init__(self):
        self.__imagePub = rospy.Publisher('/ca_slam/panorama_image', Image, queue_size=10)

    def publish(self, img, R, T):
        """
        Args:
            img (image) - fisheye image
            R (array(3 x 3)) - rotation matrix
            T (array(3 x 1)) - transition matrix
        """
        remappedImg = image_processing.remapFisheyeToPanorama(img, [320, 240], 80, 245)
        if (self.__lastImg == None):
            self.__lastImg = remappedImg
            return

        disparity = image_processing.computeDisparity(remappedImg, self.__lastImg)
        R = np.zeros((3, 3))
        T = np.array([-0.1, 0.0, 0.0])
        R1, R2, P1, P2, Q, roi1, roi2 = cv.stereoRectify(K, D, K, D, (640, 480), R, T, flags=cv.CALIB_ZERO_DISPARITY, alpha=-1, newImageSize=(640, 480))
        points, colors = image_processing.calculatePointCloudFromFisheyeDisparity(disparity, Q, img, [320, 240], 80, 235)
        msg = image_processing.convertPointsToPointCloud2MSG(points, colors, frame_id="map")
        self.__pointsPub.publish(msg)
        self.__lastImg = remappedImg