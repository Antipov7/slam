#!/usr/bin/env python

"""
Module that contains basic mathematical functions
"""

import math
import numpy as np

def rotate2D(point, angle):
    """
    Rotates a 2d point at an angle
    Args:
        point ([float, float]) - 2d point
        angle (float) - angle
    Returns:
        point ([float, float]) - rotated 2d point
    """
    cosA = np.cos(angle)
    sinA = np.sin(angle)
    rotationMatrix = np.array([[cosA, -sinA], 
                                [sinA, cosA]])
    return np.dot(rotationMatrix, point)

def getRotation2D(angle):
    """
    Returns 2d rotation matrix
    Args:
        angle (float) - angle
    Returns:
        (array) - rotation matrix
    """
    cosA = np.cos(angle)
    sinA = np.sin(angle)
    return np.array([[cosA, -sinA], 
                     [sinA, cosA]])

def getRotation3DInPlane(angle):
    """
    Returns 3d rotation matrix in the plane
    Args:
        angle (float) - angle
    Returns:
        (array) - rotation matrix
    """
    cosA = np.cos(angle)
    sinA = np.sin(angle)
    return np.array([[cosA, -sinA, 0.0], [sinA, cosA, 0.0], [0.0, 0.0, 1.0]])

def clampAngle(angle):
    """
    Limits the angle between the 0 and 2 * Pi
    Args:
        angle (float) - angle
    Returns:
        (float) - angle between the 0 and 2 * Pi
    """
    return (angle + math.pi) % (2 * math.pi) - math.pi

def addAngles(angleA, angleB):
    """
    Args:
        angleA (float) - first angle
        angleB (float) - secondAngle
    Returns:
        (float) - sum of angles between the 0 and 2 * Pi
    """
    return clampAngle(angleA + angleB)

def subAngles(angleA, angleB):
    """
    Args:
        angleA (float) - first angle
        angleB (float) - secondAngle
    Returns:
        (float) - difference between angles between the 0 and 2 * Pi
    """
    return clampAngle(angleA - angleB)

def straightLinePolarTranslation(p, alpha, tx, ty, tphi):
    """
    Straight line polar translation
    Args:
        p (float) - straight line parameter in the polar coordinate system
        alpha (float) - straight line parameter in the polar coordinate system
        tx (float) - translation distance x
        ty (float) - translation distance y
        tphi (float) - translation angle
    Returns:
        ([float, float]) - translated p and alpha
    """
    c = p + tx * math.cos(alpha) + ty * math.sin(alpha)
    translatedAlpha = alpha + tphi + math.pi * (0.5 - 0.5 * np.sign(c))
    return np.array([abs(c), clampAngle(translatedAlpha)])

def convertPolarToCartesian(p, alpha):
    """
    Convert from polar coordinate system to cartesian coordinate system
    Args:
        p (float) - straight line parameter in the polar coordinate system
        alpha (float) - straight line parameter in the polar coordinate system
    Returns:
        ([float, float]) - x and y coordinates
    """
    return np.array([p * math.cos(alpha), p * math.sin(alpha)])

def convertSphericalToCartesian(r, theta, phi):
    """
    Convert from spherical system to cartesian coordinate system
    Args:
        r (float) - radious
        theta (float) - theta angle
        phi (float) - phi angle
    Returns:
        ([float, float, float]) - x, y, z coordinates
    """
    return np.array([r * math.sin(theta) * math.cos(phi), r * math.sin(theta) * math.sin(phi), r * math.cos(theta)])

def convertCartesianToSpherical(x, y, z):
    """
    Convert from cartesian coordinate system to spherical system
    Args:
        x (float) - x coordinate
        y (float) - y coordinate
        z (float) - z coordinate
    Returns:
        ([float, float, float]) - r, theta, phi
    """
    r = math.sqrt(x * x + y * y + z * z)
    theta = math.acos(z / r)
    phi = math.atan2(y, x)
    return np.array([r, theta, phi])