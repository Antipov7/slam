#!/usr/bin/env python

"""
Module containing the functions of image processing
"""

import math
import scipy
import rospy
import numpy as np
import cv2 as cv
import yaml

class OcamInfo():
    """
    Class containing basic camera parameters
    """
    def __init__(self, directCoef, inverseCoef, center, size, affine):
        """
        Args:
            directCoef (array) - direct coefficients
            inverseCoef (array) - inverse coefficients
            center (array) - center image
            size (array) - size of image
            affine (array) - affine
        """
        self.directCoef = directCoef
        self.inverseCoef = inverseCoef
        self.center = center
        self.size = size
        self.affine = affine

class ORBMatcher():
    """
    """
    def __init__(self, maxKeyPoints = 10):
        """
        """
        self.__maxKeyPoints = maxKeyPoints
        self.__orb = cv.ORB_create(500)
        self.__bf = cv.BFMatcher(cv.NORM_HAMMING, crossCheck=True)

    def findMatches(self, firstImage, secondImage):
        """
        """
        kp1 = self.__orb.detect(secondImage, None)
        kp2 = self.__orb.detect(firstImage, None)

        kp1, des1 = self.__orb.compute(secondImage, kp1)
        kp2, des2 = self.__orb.compute(firstImage, kp2)

        matches = self.__bf.match(des1, des2)
        matches = sorted(matches, key=lambda x: x.distance)

        matchesCount = min(len(matches), self.__maxKeyPoints)
        listKp1 = [kp1[mat.queryIdx].pt for mat in matches[:matchesCount]]
        listKp2 = [kp2[mat.trainIdx].pt for mat in matches[:matchesCount]]
        return listKp1, listKp2

def loadFisheyeCameraInfo(fileName):
    """
    Loads basic camera settings
    Args:
        fileName (string) - file name
    Returns:
        (OcamInfo)
    """
    file = open(fileName)
    data = yaml.load(file)
    directCoef = np.array(data['directCoef'])
    inverseCoef = np.array(data['inverseCoef'])
    center = np.array(data['center'])
    size = np.array(data['size'])
    affineParameters = np.array(data['affineParameters'])
    file.close()
    return OcamInfo(directCoef, inverseCoef, center, size, affineParameters)

def cam2world(imagePoint, ocamInfo):
    """
    Back-projects a pixel point onto the unit sphere
    Args:
        imagePoint (array) - image point
        ocamInfo (OcamInfo) - ocam info
    Returns:
        (array) - world point in unit sphere
    """
    invdet = 1.0 / (ocamInfo.affine[0] - ocamInfo.affine[1] * ocamInfo.affine[2])
    xp = invdet * ((imagePoint[0] - ocamInfo.center[0]) - ocamInfo.affine[1] * (imagePoint[1] - ocamInfo.center[1]))
    yp = invdet * (- ocamInfo.affine[2] * (imagePoint[0] - ocamInfo.center[0]) + ocamInfo.affine[0] * (imagePoint[1] - ocamInfo.center[1]))
    
    r = math.sqrt(xp * xp + yp * yp)
    zp = ocamInfo.directCoef[0]
    r_i = 1.0
    
    for i in range(1, len(ocamInfo.directCoef)):
        r_i *= r
        zp += r_i * ocamInfo.directCoef[i]
        
    invnorm = 1.0 / math.sqrt(xp * xp + yp * yp + zp * zp)
    
    return np.array([invnorm * xp, invnorm * yp, invnorm * zp])

def world2cam(worldPoint, ocamInfo):
    """
    Projects a 3D point on to the image and returns the pixel coordinates
    Args:
        worldPoint (array) - world point
        ocamInfo (OcamInfo) - ocam info
    Returns:
        (array) - image point
    """
    norm = math.sqrt(worldPoint[0] * worldPoint[0] + worldPoint[1] * worldPoint[1])
    
    if (norm == 0):
        return np.array([ocamInfo.center[0], ocamInfo.center[1]])
    else:
        invnorm = 1.0 / norm
        t = math.atan(worldPoint[2] / norm)
        rho = ocamInfo.inverseCoef[0]
        t_i = 1.0
        
        for i in range(1, len(ocamInfo.inverseCoef)):
            t_i *= t
            rho += t_i * ocamInfo.inverseCoef[i]
        
        x = worldPoint[0] * invnorm * rho
        y = worldPoint[1] * invnorm * rho
        
        return np.array([x * ocamInfo.affine[0] + y * ocamInfo.affine[1] + ocamInfo.center[0], x * ocamInfo.affine[2] + y + ocamInfo.center[1]])

def findPlanarMotions(firstKeypoints, secondKeypoints, cameraInfo):
    """
    Compute motion estimation from two images. Movement must be scaled
    Args:
        firstKeypoints (list) - keypoints from firt image
        secondKeypoints (list) - keypoints from second image
        cameraInfo (OcamInfo) - ocam info
    Returns:
        (array) - tx, ty, theta
    """
    return scipy.optimize.least_squares(__errorMotionEstimate, [0.0, 0.0, 0.0], bounds=([-2.0, -2.0, -6.14], [2.0, 2.0, 6.14]), args=(firstKeypoints, secondKeypoints, cameraInfo)).x

def findRotation(firstImage, secondImage):
    """
    Args:
        firstImage (array) - first panoramic image
        secondImage (array) - second panoramic image
    Returns:
        (float) - angle of rotation
    """
    firstEdges = cv.Canny(firstImage, 100, 200)
    firstSum = cv.reduce(firstEdges, 0, cv.REDUCE_SUM, dtype=cv.CV_32S)[0]

    secondEdges = cv.Canny(secondImage, 100, 200)
    secondSum = cv.reduce(secondEdges, 0, cv.REDUCE_SUM, dtype=cv.CV_32S)[0]

    h,w = firstImage.shape[:2]
    return 2.0 * math.pi * np.argmax(np.correlate(firstSum, secondSum, "same")) / w - math.pi

def initFisheyePanoramaMap(ocamInfo, sf, newSize):
    """
    Computes undistortion and rectification maps for fisheye camera
    Args:
        ocamInfo (OcamInfo) - fisheye camera info
        sf (float) - zoom factor
        newSize (array) - new image size
    Returns:
        (array) - the mapping function in the x direction
        (array) - the mapping function in the y direction
    """
    w, h = newSize
    mapX = np.zeros(newSize, dtype='float32')
    mapY = np.zeros(newSize, dtype='float32')
    innerAngle = 1.0 * np.pi
    outerAngle = 2.0 * np.pi
    for i in range(w):
        for j in range(h):
            theta = float(i) / w * (outerAngle - innerAngle) + innerAngle
            phi = float(j) / h * 2.0 * np.pi
            worldPoint = np.array([np.sin(theta) * np.cos(phi), np.sin(theta) * np.sin(phi), np.cos(theta)])
            imagePoint = world2cam(worldPoint, ocamInfo)
            mapX[i, j] = imagePoint[1]
            mapY[i, j] = imagePoint[0]
    
    return mapX, mapY

def remapFisheyeToPanorama(img, mapX, mapY):
    """
    Generates a panoramic image from fisheye image
    Args:
        img (image) - image
        mapX (array) - the mapping function in the x direction
        mapY (array) - the mapping function in the y direction
    Returns:
        (image) - panoramic image
    """
    return cv.remap(img, mapX, mapY, cv.INTER_LINEAR)

def computeDisparity(imgL, imgR):
    """
    Calculates the disparity map, and apply wls filter
    Args:
        imgL (image) - first image
        imgR (image) - secondImage
    Returns:
        (img) - disparity map filtered wls filter
    """
    window_size = 5
    lmbda = 80000
    sigma = 1.2
    visual_multiplier = 1.0
    left_matcher = cv.StereoSGBM_create(minDisparity=0, numDisparities=32, blockSize=5, P1=8 * 3 * window_size ** 2, P2=32 * 3 * window_size ** 2,
    disp12MaxDiff=1, uniquenessRatio=15, speckleWindowSize=0, speckleRange=2, preFilterCap=63, mode=cv.STEREO_SGBM_MODE_SGBM_3WAY)
    right_matcher = cv.ximgproc.createRightMatcher(left_matcher)  
    wls_filter = cv.ximgproc.createDisparityWLSFilter(matcher_left=left_matcher)
    wls_filter.setLambda(lmbda)
    wls_filter.setSigmaColor(sigma)
    displ = left_matcher.compute(imgL, imgR)
    dispr = right_matcher.compute(imgR, imgL)
    displ = np.int16(displ)
    dispr = np.int16(dispr)
    filteredImg = wls_filter.filter(displ, imgL, None, dispr)
    filteredImg = cv.normalize(src=filteredImg, dst=filteredImg, beta=0, alpha=255, norm_type=cv.NORM_MINMAX)
    return np.uint8(filteredImg)

def calculatePointCloudFromFisheyeDisparity(self, disparity, q, img, centerPoint, innerRadius, outerRadius):
    """
    Args:
        disparity (image) - disparity
        q (array) - Output 4 x 4 disparity-to-depth mapping matrix
        img (image) - converted the image from a fisheye to panoramic
        centerPoint ([float, float]) - center of image unfolding
        innerRadius (float) - inner radius
        outerRadius (float) - outer radius
    Returns:
        (array([float, float])) - points with x, y, z coordinates
        (array([float, float, float])) - colors, rgb
    """
    points = []
    colors = []
    angleCoef = 1.9199 / outerRadius
    innerAngle = angleCoef * innerRadius
    outerAngle = 1.9199
    imgSize = [img.shape[1], img.shape[0]]
    for i in range(0, imgSize[1], 1):
        for j in range(0, imgSize[0], 1):
            if (disparity[i,j] == 0):
                continue
            phi = float(j) / imgSize[0] * 2.0 * np.pi
            theta = float(i) / imgSize[1] * (outerAngle - innerAngle) + innerAngle
            r =  1.0 / (disparity[i, j] / 255.0)
            points.append([np.sin(theta) * np.cos(phi) * r, np.sin(theta) * np.sin(phi) * r, np.cos(theta) * r])
            colors.append(img[i][j])
        return np.array(points), np.array(colors)

def convertPointsToPointCloud2MSG(self, points, colors, stamp=None, frame_id=None, seq=None):
    """
    Create a sensor_msgs.PointCloud2 from an array of points.
    Args:
        points (array([float, float, float])) - points with x, y, z coordinates
        colors (array([float, float, float])) - colors, rgb
        stamp (stamp) - message stamp
        frame_id (frame_id) - message frame_id
        seq (int) - message sequence ID
    Returns:
        (PointCloud2) - PointCloud2 message
    """
    msg = PointCloud2()
    assert(points.shape == colors.shape)

    buf = []

    if stamp:
        msg.header.stamp = stamp
    if frame_id:
        msg.header.frame_id = frame_id
    if seq: 
        msg.header.seq = seq
    if len(points.shape) == 3:
        msg.height = points.shape[1]
        msg.width = points.shape[0]
    else:
        N = len(points)
        xyzrgb = np.array(np.hstack([points, 1.0 - colors]), dtype=np.float32)
        msg.height = 1
        msg.width = N

    msg.fields = [
        PointField('x', 0, PointField.FLOAT32, 1),
        PointField('y', 4, PointField.FLOAT32, 1),
        PointField('z', 8, PointField.FLOAT32, 1),
        PointField('r', 12, PointField.FLOAT32, 1),
        PointField('g', 16, PointField.FLOAT32, 1),
        PointField('b', 20, PointField.FLOAT32, 1)
    ]
    msg.is_bigendian = False
    msg.point_step = 24
    msg.row_step = msg.point_step * N
    msg.is_dense = True; 
    msg.data = xyzrgb.tostring()
    return msg 

def __errorMotionEstimate(x, firstPoints, secondPoints, cameraInfo):
    error = 0.0
    c = math.cos(x[2])
    s = math.sin(x[2])
    homography = np.array([[c, -s, -x[0]], [s, c, -x[1]], [0, 0, 1]])
    for fp, sc in zip(firstPoints, secondPoints):
        p = cam2world(fp, cameraInfo)
        tp = np.dot(homography, p)
        error += np.linalg.norm(world2cam(tp, cameraInfo) - sc)
    return error