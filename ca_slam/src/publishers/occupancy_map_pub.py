#!/usr/bin/env python

"""
Module containing OccupancyGrid publisher
"""

import rospy
import math
import numpy as np
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import OccupancyGrid
from geometry_msgs.msg import Pose

class OccupancyGridPub():
    """
    Class that forms a occupancy map from a laser scan and publishes it
    """
    
    def __init__(self, resolution = 0.05, width = 800, height = 800):
        """
        Args:
            resolution (float) - map resolution
            width (int) - map width
            height (int) - map height
        """
        self.__iterTime = 100
        self.__currentIter = 0
        self.__occupancyMapPub = rospy.Publisher("/map", OccupancyGrid, queue_size = 10)
        self.__resolution = resolution
        self.__width = width
        self.__height = height
        self.__gridMap = OccupancyGrid()
        self.__gridMap.header.frame_id = "map"
        self.__gridMap.info.resolution = self.__resolution
        self.__gridMap.info.width = self.__width
        self.__gridMap.info.height = self.__height
        self.__gridMap.data = range(self.__width * self.__height)
        self.__gridMap.info.origin.position.x = - self.__width // 2 * self.__resolution
        self.__gridMap.info.origin.position.y = - self.__height // 2 * self.__resolution

        self.__grid = np.ndarray((self.__width, self.__height), buffer=np.zeros((self.__width, self.__height), dtype="int8"), dtype="int8")
        self.__grid.fill(-1)

        self.__gridMap.info.origin.position.x = -self.__width // 2 * resolution
        self.__gridMap.info.origin.position.y = -self.__height // 2 * resolution

    def updateOccupancyMap(self, robotPosition, scan):
        """
        Args:
            robotPosition ([float, float, float]) - x, y, theta coordinates
            scan (list(float)) - Laser scan
        """
        offset = np.array([robotPosition[1] // self.__resolution + self.__width // 2, robotPosition[0] // self.__resolution + self.__height // 2], dtype="float32")
        endRayPoint = np.array([0, 0], dtype="float32")
        count = len(scan)
        for (ndx, value) in enumerate(scan):
            angle = 2 * np.pi * (float(ndx) / count) + robotPosition[2]
            rotationMatrix = np.array([[np.cos(angle), np.sin(angle)] ,[-np.sin(angle), np.cos(angle)]], dtype="float32")
            if (str(value) == 'nan' or value > 10):
                value = 10
            endRayPoint[1] = value // self.__resolution
            obstacle = np.dot(rotationMatrix, endRayPoint) + offset

            if (value != 10):
                self.__updateGridCell(int(obstacle[0]), int(obstacle[1]), 0.9)
            t = 0.5
            i = 1
            freeCell = np.dot(rotationMatrix, np.array([0, t*i])) + offset
            while t * (i + 2) < endRayPoint[1]:
                self.__updateGridCell(int(freeCell[0]), int(freeCell[1]), 0.2)
                freeCell = np.dot(rotationMatrix, np.array([0, t*i])) + offset
                i += 1

        self.__currentIter += 1        
        if self.__currentIter == self.__iterTime:
            self.__currentIter = 0
            self.__publishOccupancyMap()

    def __updateGridCell(self, x, y, value):
        """
        Updates the map cell with the previous value
        """
        self.__grid[x, y] = 1.0 / (1.0 + ((1.0 - value) / value) * ((1.0 - self.__grid[x, y]) / self.__grid[x, y]))

    def __publishOccupancyMap(self):
        """
        publish occupancy map
        """
        self.__gridMap.header.stamp = rospy.Time.now()
        for i in range(self.__width * self.__height):
            if (self.__grid.flat[i] >= 0):
                self.__gridMap.data[i] = self.__grid.flat[i] * 100.0
            else:
                self.__gridMap.data[i] = self.__grid.flat[i]

        self.__occupancyMapPub.publish(self.__gridMap)