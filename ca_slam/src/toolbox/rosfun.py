#!/usr/bin/env python

"""
Module that contains basic functionality
"""

import rospy

def fetchParam(name, default):
    """
    Fetch parameter
    Args:
        name (string) - paramenter name
        default - default setting
    Returns:
        parameter value
    """
    if (rospy.has_param(name)):
        return rospy.get_param(name)
    else:
        return default 