#!/usr/bin/env python

"""
Module containing SLAM algorithm. For use must be installed: numpy version not lower then 1.13 and scipy
"""

import rospy
from ekf.ekf import Ekf
from toolbox import sio
from publishers import publishers
from image_processing import image_processing
from extractor_of_landmarks import extractor_of_clandmarks, extractor_of_cilandmarks, sensor_data
from robot_models.gazebo_mobile_robot import GazeboMobileRobot
from landmarks_database import simple_landmarks_database
from robot_models.odometry_robot import OdometryRobot
from robot_models.visual_odometry import VisualOdometry

class CaSlamPub():
    """
    Class that implements SLAM algorithm
    """
    def __init__(self):
        cameraInfoFile = sio.getFullName("camera_info.yaml")
        cameraInfo = image_processing.loadFisheyeCameraInfo(cameraInfoFile)
        self.__robot = VisualOdometry()

        self.__ekf = Ekf(self.__robot)
        self.__landmarksDatabase = simple_landmarks_database.SimpleLandmarksDatabase(1.0)
        self.__extractor = extractor_of_cilandmarks.CIExtractorOfLandmarks(cameraInfo)
        #self.__extractor = extractor_of_clandmarks.CExtractorOfLandmarks()

        self.__publishers = publishers.PublisherManager()

        self.__publishers.add(publishers.RobotMarkerPublisher())
        self.__publishers.add(publishers.RobotOdometryPublisher())
        self.__publishers.add(publishers.LandmarksPublisher())
        #self.__publishers.add(publishers.ObservedLandmarks())
        self.__publishers.add(publishers.ContourPublisher())
        #self.__publishers.add(publishers.OccupancyMapPublisher())
        self.__publishers.add(publishers.ContourFragmentsPublisher())
        self.__publishers.add(publishers.ImagePublisher())
        self.__publishers.add(publishers.ImageMatcherPublisher(20))
        self.__publishers.add(publishers.PanoramicImagePublisher())
        self.__publishers.add(publishers.FisheyePointCloudPublisher())

        #sensorData = sensor_data.LaserScanData(self.proccess)
        sensorDate = sensor_data.LaserScanAndImageData(self.proccess, cameraInfo)
        rospy.spin()
    
    def proccess(self, sensorData):
        """
        Main loop of the SLAM algorithm
        Args:
            sensorData (SensorData) - sensor data
        """
        self.__robot.update(sensorData)
        landmarks = self.__extractor.extractLandmarks(sensorData, self.__robot)
        self.__landmarksDatabase.landmarksAssociation(landmarks)
        self.__ekf.update(self.__robot, self.__landmarksDatabase, sensorData.getDeltaTime())

        self.__publishers.publish(sensorData, self.__landmarksDatabase, self.__robot)

if __name__ == '__main__':
    rospy.init_node('ca_slam')
    caSlam = CaSlamPub()