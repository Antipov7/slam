#!/usr/bin/env python

"""
Module that extracts landmarks using a contour analysis
"""

import rospy
import contour_descriptor_extractors
import contour_landmark_detectors
import numpy as np
from extractor_of_landmarks import ExtractorOfLandmarks
from contour_analisys import contour_encoding, contour_fragments
from sensor_data import LaserScanData

class CExtractorOfLandmarks(ExtractorOfLandmarks):
    """
    Extract landmarks using contour analisys
    """
    def __init__(self):
        """
        Initialization using getting parameters from rospy
        """
        self.__cornerDetector = contour_landmark_detectors.initCornerDetectorUsingRospy()
        self.__cornerDescriptorExctractor = contour_descriptor_extractors.initCornerDescriptorExtractorUsingRospy()
        self.__straightLineDetector = contour_landmark_detectors.initStraightLineDetectorUsingRospy()
        self.__straightLineDescriptorExctractor = contour_descriptor_extractors.initStraightLineDescriptorExtractorUsingRospy()

    def extractLandmarks(self, laserScanData, robotPosition):
        """
        Extract landmarks
        Args:
            laserScanData (LaserScanData) - laser scan data
            robotPosition ([float, float, float]) - x, y, theta position of robot
        Returns:
            list(landmark) - landmarks
        """
        #if not isinstance(laserScanData, LaserScanData):
            #raise ValueError("laserScanData must be type LaserScanData. Check the configuration slam algorithm")
        
        landmarks = []
        laserScan = laserScanData.getLaserScan()
        ranges = np.clip(laserScan.ranges, -1, laserScan.range_max)
        scan = ranges[ranges > 0.01]
        polarCode = contour_encoding.convertLaserScanToPolarCode(scan)
        differenceCode = contour_encoding.convertPolarCodeToDifferenceCode(polarCode)
        fragments = contour_fragments.extractContourFragments(differenceCode, scan[0], [0.0, 0.0], 1)
        cornerLandmarks = self._cornerLandmarkDetect(fragments, robotPosition)
        straightLineLandmarks = self._straightLineDetect(fragments, robotPosition)
        if (cornerLandmarks is None):
            landmarks = straightLineLandmarks
        elif (straightLineLandmarks is None):
            landmarks = cornerLandmarks
        elif (cornerLandmarks is not None and straightLineLandmarks is not None):
            landmarks = cornerLandmarks + straightLineLandmarks
        return landmarks

    def _cornerLandmarkDetect(self, contourFragments, robotPosition):
        """
        Args:
            contourFragments (list(ContourFragment)) - contour fragments
            robotPosition ([float, float, float]) - x, y, theta position of robot 
        Returns:
            (list(CornerLandmark)) - landmarks
        """
        cornerLandmarks, fragments, ndxs = self.__cornerDetector.detect(contourFragments, robotPosition)
        if (cornerLandmarks is None):
            return None 
        for i, landmark in enumerate(cornerLandmarks):
            landmark.descriptor = self.__cornerDescriptorExctractor.compute(fragments[i], ndxs[i], robotPosition)
        return cornerLandmarks

    def _straightLineDetect(self, contourFragments, robotPosition):
        """
        Args:
            contourFragments (list(ContourFragment)) - contour fragments
            robotPosition ([float, float, float]) - x, y, theta position of robot 
        Returns:
            (list(CornerLandmark)) - landmarks
        """
        lineLandmarks, fragments, lines = self.__straightLineDetector.detect(contourFragments, robotPosition)
        if (lineLandmarks is None):
            return None
        for i, landmark in enumerate(lineLandmarks):
            landmark.descriptor = self.__straightLineDescriptorExctractor.compute(fragments[i].pointCloud, lines[i], robotPosition)
        return lineLandmarks