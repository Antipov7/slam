#!/usr/bin/env python

import unittest
import contour_filtering
import numpy as np
from math import pi

class TestContourFiltering(unittest.TestCase):

    def test_selectLinearFragmentsIsCircular(self):
        contour = [1 + 1j, 1 + 1j, 1 + 1j, 1, 1, 1, 1, 1, 1 - 1j, 1 - 1j, 1 - 1j, -1j, -1 - 1j, -1, -1 + 1j, -1 + 1j, -1 - 1j, -1, -1 -1j, -1 + 1j, 1j, -1, -1 -1j, -1 + 1j]
        normSignal = np.array([0.2, 0.555, 0.85, 0.828, 0.866, 1, 0.866, 0.828, 0.85, 0.8, 0.644, 0.4, 0.2, 0.4, 0.555, 0.65, 0.555, 0.555, 0.4, 0.485, 0.4, 0.5, 0.2, 0.111])

        result = contour_filtering.selectLinearFragments(contour, 5)
        np.testing.assert_almost_equal(result, normSignal, 2)

    def test_selectLinearFragments(self):
        contour = [1 + 1j, 1 + 1j, 1 + 1j, 1, 1, 1, 1, 1, 1 - 1j, 1 - 1j, 1 - 1j, -1j, -1 - 1j, -1, -1 + 1j, -1 + 1j, -1 - 1j, -1, -1 -1j, -1 + 1j, 1j, -1, -1 -1j, -1 + 1j]
        normSignal = np.array([0, 0, 0.85, 0.828, 0.866, 1, 0.866, 0.828, 0.85, 0.8, 0.644, 0.4, 0.2, 0.4, 0.555, 0.65, 0.555, 0.555, 0.4, 0.485, 0.4, 0.5, 0, 0])

        result = contour_filtering.selectLinearFragments(contour, 5, False)
        np.testing.assert_almost_equal(result, normSignal, 2)

        normSignal = np.array([0, 0, 0, 0.966, 0.981, 0.907, 0.981, 0.966, 0.866, 0.621, 0.439, 0.222, 0.111, 0.277, 0.560, 0.694, 0.694, 0.555, 0.560, 0.560, 0.560, 0, 0, 0] )

        result = contour_filtering.selectLinearFragments(contour, 6, False)
        np.testing.assert_almost_equal(result, normSignal, 2)

    def test_selectLinearFragmentsThrowExceptions(self):
        contour = [1 + 1j, 1 + 1j, 1 + 1j, 1, 1, 1, 1, 1, 1 - 1j, 1 - 1j, 1 - 1j, -1j, -1 - 1j, -1, -1 + 1j, -1 + 1j, -1 - 1j, -1, -1 -1j, -1 + 1j, 1j, -1, -1 -1j, -1 + 1j]

        self.assertRaises(ValueError, contour_filtering.selectLinearFragments, contour, 100)

    def test_selectAngularFragmentsIsCircular(self):
        contour = [1, 1, 1, 1, 1, 1, 1, 1, -1j, -1j, -1j, -1j, -1j, -1j, -1j, -1j]
        normSignal = np.array([0, 0.141, 0.282, 0.447, 0.632, 0.824, 0.905, 1, 1, 0.905, 0.824, 0.632, 0.447, 0.282, 0.141, 0])

        result = contour_filtering.selectAngularFragments(np.array(contour), -pi/2, 10)
        np.testing.assert_almost_equal(result, normSignal, 2)

    def test_selectAngularFragmentsThrowsExceptions(self):
        contour = [1, 1, 1, 1, 1, 1, 1, 1, -1j, -1j, -1j, -1j, -1j, -1j, -1j, -1j]

        self.assertRaises(ValueError, contour_filtering.selectAngularFragments, contour, -pi/2, 7, False)
        self.assertRaises(ValueError, contour_filtering.selectAngularFragments, contour, -pi/2, 100, False)