#!/usr/bin/env python

"""
Module which contains descriptors for landmarks
"""

import rospy
import numpy as np
import cv2 as cv
from toolbox import smath
from abc import ABCMeta, abstractmethod

class Descriptor():
    """
    Abstract class descriptor
    """
    __metaclass__ = ABCMeta

    def match(self, descriptor):
        """
        Returns the distance between descriptors. The smaller the value, the descriptors are similar.
        Args:
            descriptor (Descriptor) - other descriptor
        """
        if not isinstance(descriptor, type(self)):
            return float('inf')
        return self._featureMatch(descriptor)

    @abstractmethod
    def _featureMatch(self, descriptor):
        """
        Returns the distance between descriptors. The smaller the value, the descriptors are similar.
        Args:
            descriptor (Descriptor) - other descriptor
        """
        pass

class CornerDescriptor(Descriptor):
    """
    Abstract class corner descriptor
    """
    
    @abstractmethod
    def __init__(self, contourFragment, ndx, robotPosition):
        """
        Args:
            contourFragment (ContourFragment) - contour fragment
            ndx (int) - index position of the landmark on the contour fragment
            robotPosition ([float, float, float]) - x, y, theta position of robot 
        """
        pass

class StraightLineDescriptor(Descriptor):
    """
    Abstract class straight line descriptor
    """

    @abstractmethod
    def __init__(self, pointCloud, landmarkPolarParameters):
        """
        Args:
            pointCloud (array([float, float])) - point cloud
            landmarkPolarParameters ([float, float]) - r, alpha parameters
        """
        pass

class ImageDescriptor(Descriptor):
    """
    """
    def __init__(self, feature):
        self.__features = feature

    def _featureMatch(self, descriptor):
        """
        Returns the distance between descriptors. The smaller the value, the descriptors are similar.
        Args:
            descriptor (Descriptor) - other descriptor
        """
        if self.__features - descriptor.__features == 0:
            return 0
        return 100000

class PlaceCornerDescription(CornerDescriptor):
    """
    Descriptor that uses only landmark position as feature
    """
    def __init__(self, contourFragment, ndx, robotPosition):
        """
        Args:
            contourFragment (ContourFragment) - contour fragment
            ndx (int) - index position of the landmark on the contour fragment
            robotPosition ([float, float, float]) - x, y, theta position of robot 
        """
        transposeMatrix = np.array([robotPosition[0], robotPosition[1]])
        self.__features = smath.rotate2D(contourFragment.pointCloud[ndx], -robotPosition[2]) + transposeMatrix

    def _featureMatch(self, descriptor):
        """
        Returns the distance between descriptors. The smaller the value, the descriptors are similar.
        Args:
            descriptor (Descriptor) - other descriptor
        """
        return np.linalg.norm(self.__features - descriptor.__features)

class PolarParametersStraightLineDescriptor(StraightLineDescriptor):
    """
    Descriptor that uses only polar parameters as feature
    """

    def __init__(self, landmarkPolarParameters):
        """
        Args:
            landmarkPolarParameters ([float, float]) - r, alpha parameters
        """
        self.__features = landmarkPolarParameters

    def _featureMatch(self, descriptor):
        """
        Returns the distance between descriptors. The smaller the value, the descriptors are similar.
        Args:
            descriptor (Descriptor) - other descriptor
        """
        dist = self.__features - descriptor.__features
        dist[0] = dist[0] * 0.9
        dist[1] = dist[1] / (0.1 * 6.28)
        dist = np.linalg.norm(dist)
        return dist