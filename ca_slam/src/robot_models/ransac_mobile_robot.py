#!/usr/bin/env python

"""
Module describing the kinematic model of the robot using an RANSAC algorithm
"""

import math
import rospy
import numpy as np
from robot_model import RobotModel

class RansacMobileRobot(RobotModel):
    """
    Class describing the kinematic motion of the robot
    """
    def __init__(self):
        super(RansacMobileRobot, self).__init__()
        self._wheelRadious = 0.332 / 2.0
        self._stateTransitionMatrix = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]], dtype="float32")
        self._controlMatrix = np.array([[0, 0], [0, 0], [0, 0]], dtype="float32")
        self._controlCovarianceMatrix = np.zeros((2, 2))
        self._covarianceMatrix = np.diag([0, 0, 0])
        self.__previousLandmarks = None

    def update(self, landmarkDatabase):
        """
        Update position, stateTransitionMatrix, controlMatrix
        Args:
            dt (float) - delta time between two consecutive lidar messages (must be seconds)
        """
        if (self.__previousLandmarks != None):
            pairs = np.array(self.__getPairsPoint(landmarkDatabase))
            if (len(pairs) > 3):
                transitionMatrix, bestPoints = self.__getTransitionMatrix2DRANSAC(pairs, 3, 100, 0.001)
                if (transitionMatrix is not None):
                    theta, dist = self.__getTravel(transitionMatrix)

                    self._position[0] += dist * math.cos(self._position[2] + 0.5 * theta)
                    self._position[1] += dist * math.sin(self._position[2] + 0.5 * theta)
                    self._position[2] += theta

        self.__previousLandmarks = []
        for landmark in landmarkDatabase.getObservedLandmarks():
            self.__previousLandmarks.append(landmark)

    def getStatePrediction(self):
        """
        Returns state prediction
        Returns:
            ([float, float, float]) - x, y, theta coordinates
        """
        return self._position      

    def getTransitionMatrix(self):
        """
        Returns transition matrix
        Returns:
            ([[float, float, float], [float, float, float], [float, float, float]]) - Jacobian matrix ([[dstatex / dx, dstatex / dy, dstatex / dtheta], [dstatey / dx, dstatey / dy, dstatetheta / dtheta], [dstatez / dx, dstatez / dy, dstatetheta / dtheta]])
        """
        return self._stateTransitionMatrix

    def getControlMatrix(self):
        """
        Returns control matrix
        Returns:
            ([[float, float], [float, float], [float, float]]) - Jacobian matrix ([[dstatex / dvx, dstatex / dvtheta], [dstatey / dvx, dstatey / dvtheta] ,[dstatetheta / dvx, dstatetheta / dvtheta]])
        """
        return self._controlMatrix

    def getCovarianceMatrix(self):
        """
        Return covariance error matrix
        Returns:
            ([[float, float, float], [float, float, float], [float, float, float]])
        """
        return self._covarianceMatrix

    def getControlCovarianceMatrix(self):
        """
        Return control covariance error matrix
        Returns:
            ([[float, float], [float, float]])
        """
        return self._controlCovarianceMatrix

    def setState(self, state):
        """
        Set state
        Args:
            state ([float, float, float]) - x, y, theta coordinates
        """
        self._position = state

    def setCovarianceMatrix(self, covariance):
        """
        Set covariance
        Args:
            covariance ([[float, float, float], [float, float, float], [float, float, float]]) - covariance matrix
        """
        self._covarianceMatrix = covariance

    def __getPairsPoint(self, landmarkDatabase):
        """
        """
        pairs = []
        landmarks = landmarkDatabase.getObservedLandmarks()
        for landmark in landmarks:
            bestDist = 10000000
            pair = None
            for previousLandmark in self.__previousLandmarks:
                dist = math.sqrt( math.pow(previousLandmark.getGlobalPosition()[0] - landmark.getGlobalPosition()[0] ,2) + math.pow(previousLandmark.getGlobalPosition()[1] - landmark.getGlobalPosition()[1] ,2))
                if (bestDist > dist):
                    pair = [[landmark.getGlobalPosition()[0], landmark.getGlobalPosition()[1]], [previousLandmark.getGlobalPosition()[0], previousLandmark.getGlobalPosition()[1]]]
                    bestDist = dist        
            if (pair != None):
                pairs.append(pair)
        return pairs

    def __getTransitionMatrix2DRANSAC(self, pairs, sampleSubset = 3, iterations = 100, errorThreshold = 0.5, maxTransferRestriction = 0.3):
        """
        Calculate RT(theta) matrix using RANSAC
        pairs - Pairs of points
        sampleSubset - Number of pairs of points to calculate transition matrix
        iterations - Number of iterations
        errorThreshold - Threshold that determine if a data sample agrees with a model or not
        maxTransferRestriction - Maximum radius of transfer restriction
        """
        bestTransitionMatrix = None
        bestDistance = 100000000
        bestInliers = 0
        bestPairs = None
        for i in range(iterations):
            selectedPairs = np.array(self.__getRandomPairs(pairs, sampleSubset))
            currentPoints = np.array(selectedPairs[:, 1])
            previousPoints = np.array(selectedPairs[:, 0])
            transition = self.__getTransitionMatrix2D(currentPoints, previousPoints)
            inliers, distance, points = self.__getEstimateTransitionMatrix2D(np.array(pairs), transition, errorThreshold)
            if inliers != 0:
                meanDistance = distance / inliers
                if inliers > bestInliers and meanDistance < bestDistance and self.__checkTransitionMatrixConstraints(transition, maxTransferRestriction):
                    bestDistance = meanDistance
                    bestTransitionMatrix = transition
                    bestInliers = inliers
                    bestPairs = points

        return bestTransitionMatrix, np.array(bestPairs)

    def __getTransitionMatrix2D(self, currentPoints, previousPoints):
        """
        Return RT(theta) matrix from the previous to the current points in the two-dimensional coordinates. Method of least squares
        currentPoints - transformed points
        previousPoints - previous points 
        """
        count = previousPoints.shape[0]
        sx = sum(previousPoints[:, 0]) / count
        sy = sum(previousPoints[:, 1]) / count
        su = sum(currentPoints[:, 0]) / count
        sv = sum(currentPoints[:, 1]) / count

        sxx, syy, sxy, sxu, syu, sxv, syv = 0, 0, 0, 0, 0, 0, 0
        for i in range(count):
            dx = previousPoints[i, 0] - sx
            dy = previousPoints[i, 1] - sy
            du = currentPoints[i, 0] - su
            dv = currentPoints[i, 1] - sv
            sxx += dx*dx
            syy += dy*dy
            sxy += dx*dy
            syv += dy*dv
            sxv += dx*dv
            syu += dy*du
            sxu += dx*du

        rt = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]], dtype='f')
        dxy = syy * sxx - sxy * sxy
        if (abs(dxy) > 0):
            rt[0, 0] = (syy*sxu - sxy*syu) / dxy
            rt[0, 1] = (syu*sxx - sxy*sxu) / dxy
            rt[0, 2] = su - sx * rt[0, 0] - sy * rt[0, 1]
            rt[1, 0] = (syy*sxv - sxy*syv) / dxy
            rt[1, 1] = (syv*sxx - sxy*sxv) / dxy
            rt[1, 2] = sv - sx * rt[1, 0] - sy * rt[1,  1]
    
        return rt

    def __checkTransitionMatrixConstraints(self, transitionMatrix, maxTransferRestriction):
        """
        Verifies that the matrix matches the constraints
        transitionMatrix - RT(theta) matrix from the previous to the current points in the two-dimensional coordinates
        maxTransferRestriction - Maximum radius of transfer restriction
        """
        dist = np.linalg.norm([transitionMatrix[0, 2], transitionMatrix[1, 2]])
        return dist <= maxTransferRestriction

    def __getEstimateTransitionMatrix2D(self, pairs, transitionMatrix, threshold):
        """
        Returns the transfer matrix estimate, by matching between pairs of points
        pairs - Pairs of points
        transitionMatrix - RT(theta) matrix from the previous to the current points in the two-dimensional coordinates
        threshold - threshold for calculating an inliers
        """
        distance = 0
        inliersCount = 0
        inliers = []
        for pair in pairs:
            x = pair[0, 0] * transitionMatrix[0, 0] + pair[0, 1] * transitionMatrix[0, 1] + transitionMatrix[0, 2]
            y = pair[0, 0] * transitionMatrix[1, 0] + pair[0, 1] * transitionMatrix[1, 1] + transitionMatrix[1, 2]
            distanceBeetwenPoints = self.__getDistanceBeetwenPoints(pair[1, :], [x, y])
            distance += distanceBeetwenPoints
            if (distanceBeetwenPoints <= threshold):
                inliers.append(pair)
                inliersCount += 1
    
        return inliersCount, distance, inliers

    def __getRandomPairs(self, pairs, count):
        """
        Returns random pairs of points
        pairs - Pairs of points
        count - Number of pairs
        """
        selectedPairs = []
        pairsNdxs = np.random.choice(range(len(pairs)), count, replace=False)
        for ndx in pairsNdxs:
            selectedPairs.append(pairs[ndx])
        return selectedPairs

    def __getDistanceBeetwenPoints(self, pointA, pointB):
        """
        Return the distance between two points
        """
        return math.pow(np.linalg.norm(pointB - pointA), 2)

    def __getTravel(self, transitionMatrix):
        """
        Returns the rotation angle and distance
        transitionMatrix - RT(theta) matrix from the previous to the current points in the two-dimensional coordinates
        """
        n1 = np.linalg.norm([transitionMatrix[0, 0], transitionMatrix[1, 0]])
        n2 = np.linalg.norm([transitionMatrix[0, 1], transitionMatrix[1, 1]])

        theta = math.acos((transitionMatrix[0, 0] / n1 + transitionMatrix[1, 1] / n2) / 2.0)
        if transitionMatrix[0, 1] > 0:
            theta = -theta

        dist = np.linalg.norm([transitionMatrix[0, 2], transitionMatrix[1, 2]])
        return theta, dist