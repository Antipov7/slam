#!/usr/bin/env python

"""
Module containing interfaces associated with the extraction of landmarks
"""

import math
import numpy as np
from abc import ABCMeta, abstractmethod
from sensor_msgs.msg import LaserScan

class ExtractorOfLandmarks:
    """
    Abstract class on the extraction of landmarks
    """
    __metaclass__  = ABCMeta

    @abstractmethod
    def extractLandmarks(self, laserScan, laserScanPosition, robotPosition):
        """
        Extract landmarks
        Args:
            laserScan (list(float)) - laser scan by lidar
            laserScanPosition ([float, float]) - position of lidar
        Returns:
            list(landmark) - landmarks
        """
        pass