#!/usr/bin/env python

"""
Module that contains class describing the map that is used in the Extended Kalman Filter
"""

import numpy as np

class EkfMap(object):
    """
    Map that contains the state vector and covariance
    """

    def __init__(self, robot):
        """
        Initialization state, covariance and transition matrix of the system. matrixBlocks contains a pair of values: position and size of matrix blocks that correspond to systemObjects
        Args:
            robot (EkfSystemModel) - model describing robot
        """
        self.__state = np.copy(robot.getStatePrediction())
        self.__covariance = np.copy(robot.getCovarianceMatrix())
        self.__matrixBlocks = {robot.getId():[0, self.__state.shape[0]]}

    @property
    def state(self):
        return self.__state

    @state.setter
    def state(self, state):
        self.__state = state

    @property
    def covariance(self):
        return self.__covariance

    @covariance.setter
    def covariance(self, covariance):
        self.__covariance = covariance

    def getMapSize(self):
        """
        Returns map size
        Returns:
            (int) - map size
        """
        return self.covariance.shape[0]

    def addSystemObjectToMap(self, systemObject):
        """
        Add a new object to the map. System object can be robot, a landmark, etc. Those objects that are on the map
        Args:
            systemObject (EkfSystemModel) - system object
        """
        self.state = np.append(self.state, systemObject.getStatePrediction())
        covariance = systemObject.getCovarianceMatrix()
        position = self.covariance.shape[0]
        size = covariance.shape[0]
        self.__matrixBlocks[systemObject.getId()] = [position, size]
        self.covariance = self.__insertMatrix(self.covariance, covariance)

    def getPositionMatrixBlockOfSystemObject(self, id):
        """
        Returns the position and size of the matrix block of the system object
        Args:
            id (int) - id of system object
        Returns:
            ([float, float]) - position and size 
        """
        return self.__matrixBlocks[id]

    def getStateOfSystemObject(self, id):
        """
        Returns state vector of system object
        Args:
            id (int) - id of system object
        Returns:
            (array) - state vector
        """
        position = self.__matrixBlocks[id][0]
        size = self.__matrixBlocks[id][1]
        return self.state[position:position + size]

    def getCovarianceOfSystemObject(self, id):
        """
        Returns covariance of system object
        Args:
            id (int) - id of system object
        Returns:
            (array) - covariance
        """
        position = self.__matrixBlocks[id][0]
        size = self.__matrixBlocks[id][1]
        return self.covariance[position:position + size, position:position + size]

    def getCovarianceBetweenSystemObjects(self, firstId, secondId):
        """
        Returns covariance between system objects
        Args:
            firstId (int) - id of first system object
            secondId (int) - id of second system object
        Returns:
            (array) - covariance
        """
        x = self.__matrixBlocks[firstId][0]
        y = self.__matrixBlocks[secondId][0]
        width = self.__matrixBlocks[firstId][1]
        height = self.__matrixBlocks[secondId][1]
        return self.covariance[x : x + width, y : y + height]

    def setCovarianceBetweenSystemObjects(self, covariance, firstId, secondId):
        """
        Set covariance between two system objects
        Args:
            covariance (array) - covariance between system objects
            firstId (int) - id of first system object
            secondId (int) - id of second system objects
        """
        x = self.__matrixBlocks[firstId][0]
        y = self.__matrixBlocks[secondId][0]
        width = self.__matrixBlocks[firstId][1]
        height = self.__matrixBlocks[secondId][1]
        self.covariance[x : x + width, y : y + height] = covariance

    def mergeMap(self, otherMap):
        """
        Copies value from map
        Args:
            otherMap (EkfMap) - map from which values are copied
        """
        ids = otherMap.__matrixBlocks.keys()
        for objectId in ids:
            position, size = self.__matrixBlocks[objectId]
            self.state[position : position + size] = otherMap.getStateOfSystemObject(objectId)

        for firstId in ids:
            for secondId in ids:
                self.setCovarianceBetweenSystemObjects(otherMap.getCovarianceBetweenSystemObjects(firstId, secondId), firstId, secondId)

    def __insertMatrix(self, firstMatrix, secondMatrix):
        """
        Insert matrix into matrix
        Args:
            firstMatrix (array) - first matrix into which second matrix is inserter
            secondMatrix (array) - second matrix
        Returns:
            (array) - matrix of the form [[firstMatrix, 0], [0, secondMatrix]]
        """
        a = np.zeros((firstMatrix.shape[0], secondMatrix.shape[1]))
        b = np.zeros((secondMatrix.shape[0], firstMatrix.shape[1]))
        return np.block([[firstMatrix, a], [b, secondMatrix]])