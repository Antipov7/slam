#!/usr/bin/env python

"""
A module that uses odometry messages to replace the kinematic model
"""

import math
import rospy
import numpy as np
from toolbox import smath
from robot_model import RobotModel
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry

class OdometryRobot(RobotModel):
    """
    Class describing the kinematic motion of the robot
    """
    def __init__(self):
        super(OdometryRobot, self).__init__()
        self.__twistSub = rospy.Subscriber('cmd_vel', Twist, self.twistCallback, queue_size=10)
        self.__odomSub = rospy.Subscriber('/odom', Odometry, self.poseCallback)

        self._wheelRadious = 0.332 / 2.0
        self.__velocity = np.array([0, 0, 0, 0, 0, 0], dtype="float32")
        self._stateTransitionMatrix = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]], dtype="float32")
        self._controlMatrix = np.array([[0, 0], [0, 0], [0, 0]], dtype="float32")
        self._controlCovarianceMatrix = np.zeros((2, 2))
        self._covarianceMatrix = np.diag([0, 0, 0])

    def twistCallback(self, msg):
        """
        Receives and saves the twist message
        Args:
            msg (Twist) - twist message
        """
        self.__velocity[0] = msg.linear.x
        self.__velocity[1] = msg.linear.y
        self.__velocity[2] = msg.linear.z
        self.__velocity[3] = msg.angular.x
        self.__velocity[4] = msg.angular.y
        self.__velocity[5] = -msg.angular.z

    def poseCallback(self, msg):
        """
        Keeps the position of the robot
        Args:
            msg (Odometry) - odometry msg
        """
        self._position[0] = msg.pose.pose.position.x
        self._position[1] = msg.pose.pose.position.y
        if (msg.pose.pose.orientation.z >= 0):
            self._position[2] = smath.clampAngle(- 2.0 * math.acos(msg.pose.pose.orientation.w))
        else:
            self._position[2] = smath.clampAngle(2.0 * math.acos(msg.pose.pose.orientation.w))

    def update(self, dt):
        """
        Update position, stateTransitionMatrix, controlMatrix
        Args:
            dt (float) - delta time between two consecutive lidar messages (must be seconds)
        """
        velocity = np.copy(self.__velocity)

        self._stateTransitionMatrix[2, 0] = -velocity[0] * dt * math.sin(self._position[2] - 0.5 * velocity[5] * self._wheelRadious * dt)
        self._stateTransitionMatrix[2, 1] = velocity[0] * dt * math.cos(self._position[2] - 0.5 * velocity[5] * self._wheelRadious * dt)

        self._controlMatrix[0, 0] = dt * math.cos(self._position[2] - 0.5 * velocity[5] * self._wheelRadious * dt)
        self._controlMatrix[0, 1] = 0.5 * velocity[0] * self._wheelRadious * dt * dt * math.sin(self._position[2] - 0.5 * velocity[5] * self._wheelRadious * dt)
        self._controlMatrix[1, 0] = dt * math.sin(self._position[2] - 0.5 * velocity[5] * self._wheelRadious * dt)
        self._controlMatrix[1, 1] = -0.5 * velocity[0] * self._wheelRadious * dt * dt * math.cos(self._position[2] - 0.5 * velocity[5] * self._wheelRadious * dt)
        self._controlMatrix[2, 0] = 0.0
        self._controlMatrix[2, 1] = -self._wheelRadious * dt

    def getStatePrediction(self):
        """
        Returns state prediction
        Returns:
            ([float, float, float]) - x, y, theta coordinates
        """
        return self._position      

    def getTransitionMatrix(self):
        """
        Returns transition matrix
        Returns:
            ([[float, float, float], [float, float, float], [float, float, float]]) - Jacobian matrix ([[dstatex / dx, dstatex / dy, dstatex / dtheta], [dstatey / dx, dstatey / dy, dstatetheta / dtheta], [dstatez / dx, dstatez / dy, dstatetheta / dtheta]])
        """
        return self._stateTransitionMatrix

    def getControlMatrix(self):
        """
        Returns control matrix
        Returns:
            ([[float, float], [float, float], [float, float]]) - Jacobian matrix ([[dstatex / dvx, dstatex / dvtheta], [dstatey / dvx, dstatey / dvtheta] ,[dstatetheta / dvx, dstatetheta / dvtheta]])
        """
        return self._controlMatrix

    def getCovarianceMatrix(self):
        """
        Return covariance error matrix
        Returns:
            ([[float, float, float], [float, float, float], [float, float, float]])
        """
        return self._covarianceMatrix

    def getControlCovarianceMatrix(self):
        """
        Return control covariance error matrix
        Returns:
            ([[float, float], [float, float]])
        """
        self._controlCovarianceMatrix[0, 0] = (0.9 * self.__velocity[0]) * (0.9 * self.__velocity[0])
        self._controlCovarianceMatrix[1, 1] = (0.9 * self.__velocity[5]) * (0.9 * self.__velocity[5])
        return self._controlCovarianceMatrix

    def setState(self, state):
        """
        Set state
        Args:
            state ([float, float, float]) - x, y, theta coordinates
        """
        pass

    def setCovarianceMatrix(self, covariance):
        """
        Set covariance
        Args:
            covariance ([[float, float, float], [float, float, float], [float, float, float]]) - covariance matrix
        """
        pass