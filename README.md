# Introduction
The SLAM means simultaneous localization and mapping. The problem of SLAM is connected with the construction of a map of unknown space by a mobile robot during navigation on this map. This problem is a fundamental task for the autonomous robot constructing and is the basic method for most navigation systems.

This SLAM algorithm is based on the extented Kalman filter for ROS (Robot Operation System). SLAM algorithm can be implemented in various ways. SLAM is more like a concept than a single algorithm. There are many parts involved in SLAM, and these various parts can be implemented using a number of different algorithms.

In this implementation of the algorithm, the following parts are implemented:

* Two types of landmark "corner" and "image feature"

* 4 types of detector using contour analysis to process lidar data: 
    * Matched filtering method
    * Wu algorithm
    * Teh-Chin algorithm
    * Curvature scale space method
    
* 2 advanced algorithms based on the extended Kalman filter: divide and conquer EKF-SLAM algorithm and algorithm of EKF-SLAM with adaptive range observations

* Algorithm for constructing a three-dimensional environmental scene using a fisheye camera

# Requirements
* Ubuntu 16.04.6 LTS amd 64
* ROS Kinetic
* numpy 1.13
* scipy
* openCV 3.0

# ROS API
## Topics
### Subscribed Topics
*/scan* (sensor_msgs/LaserScan) - Scans from a laser range-finder

*/raspicam_node/image_raw* (sensor_msgs/Image) - Image from a fisheye camera .Test topic, an algorithm that will use it in development

*/cmd_vel* (geometry_msgs/Twist) - Control topic - sets linear and angular velocity

### Published Topics

*/ca_slam_odom* (nav_msgs/Odometry) - Odometry messages

*/robotMarker* (visualization_msgs/Marker) - Robot position messages

*/landmarks* (sensor_msgs/PointCloud) - Message containing all lidar-detected landmarks

*/observedLandmarks* (sensor_msgs/PointCloud) - Message containing all observed lidar-detected landmarks

*/pointCloud* (sensor_mesg/PointCloud) - Message containing lidar outline using contour analysis

# ROS Parameters

_~cornerDetector_ (string, default: "matchedFiltering") - set "corner" type landmark detector

* matchedFiltering - matched filtering method

    * _~matchedFilteringSize_ (int, default: 6) - filter window size. Window size must be an even number
        
    * _~matchedFilteringThreshold_ (float, default: 0.6) - threshold at which the detection occurs. Must be in the range of 0 to 1
        
* curvatureWu - algorithm Wu
    
    * _~curvatureWuSize_ (int, default: 12) - window size
        
    * _~curvatureWuThreshold_ (float, default: 0.25) - threshold at which the detection occurs. Must be in the range of 0 to 2
    
* curvatureTehChin - algorithm Teh-Chin

    * _~curvatureTehChinSize_ (int, default: 10) - minimum contour size that will be processed by the algorithm
    
    * _~curvatureTehChinThreshold_ (float, default: 0.25) - threshold at which the detection occurs
    
* multiscaleDominant - multiscale dominant point detector

    * _~multiscaleDominantSize_ (int, default: 6) - aperture size
    
    * _~multiscaleDominantSigma_ (float, default: 2) - standard deviation
    
* none - detector is not used

_~localMapping_ (string, default: "globalMapping") - set local mapping algorithm

* globalMapping - EKF-SLAM

* AR - AR-SLAM

* DAC - DAC-SLAM

# Results

https://youtu.be/0rHM61hePAg
![Alt text](/images/1.png)
![Alt text](/images/2.png)
![Alt text](/images/3.png)
![Alt text](/images/4.png)
![Alt text](/images/5.png)
![Alt text](/images/6.png)