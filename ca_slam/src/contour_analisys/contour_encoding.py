#!/usr/bin/env python

"""
The module contains functions for encoding a contour
"""

import rospy
import math
import numpy as np
from toolbox import smath
from sensor_msgs.msg import PointCloud
from geometry_msgs.msg import Point32
from math import pi

def convertLaserScanToPolarCode(scan, endAngle=2 * pi):
    """
    Converts the laser scan into a polar code
    Args:
        scan (list(float)) - Laser scan
        endAngle (float) - Maximum scannig angle
    Returns:
        (array(complex)) - contour is represented by a polar code
    """
    count = len(scan)
    contour = np.zeros(count, dtype='complex128')
    for (ndx, value) in enumerate(scan):
        angle = endAngle * (float(ndx) / count)
        contour[ndx] = complex(value * math.cos(angle), value * math.sin(angle))
    return contour

def convertPolarCodeToDifferenceCode(polarCode):
    """
    Converts the polar code into an difference code
    Args:
        polarCode (array(complex)) - contour is represented by a polar code
    Returns:
        array(complex) - contour is represented by a difference code
    """
    return np.diff(polarCode)
        
def equivalenceOfContourCode(contourCode, approximateLengthSegments):
    """
    Equalization of contour code
    Args:
        contourCode (array(complex)) - Difference code
        approximateLengthSegments (float) - Approximate length of segments
    Returns:
        array(complex) - equalization contour
    """
    if len(contourCode) == 0:
        return []
    lengthSegment, countSegments =  __getLengthOfTheSegments(contourCode, approximateLengthSegments)
    equivalenceCode = np.zeros(countSegments, dtype='complex128')
    remainder = contourCode[0]
    n = 0

    for i in range(countSegments):
        if (abs(remainder) >= lengthSegment):
            equivalenceCode[i] = (lengthSegment * remainder / abs(remainder))
            remainder = remainder - equivalenceCode[i]
        else:
            sum = 0
            t = 0
            while (abs(remainder) + sum < lengthSegment and n + t + 1 < len(contourCode)):
                t += 1
                sum += abs(contourCode[n + t])
            
            sum = 0
            for l in range(1, t):
                sum += abs(contourCode[n + l])
            
            moduleUsedPart = lengthSegment - abs(remainder) - sum
            usedPart = contourCode[n + t] * moduleUsedPart / abs(contourCode[n + t])
        
            sumValue = 0
            for l in range(1, t):
                sumValue += contourCode[n + l]

            equivalenceCode[i] = remainder + usedPart + sumValue
            remainder = contourCode[n + t] - usedPart
            n += t

    return equivalenceCode

def convertContourCodeToPointCloud(contourCode, firstPoint):
    """
    Convert the contour into a point cloud
    Args:
        contourCode (array(complex)) - Difference equivalence code
        firstPoint (array(float, float)) - The first point of contour
    Returns:
        array([float, float]) - pointCloud
    """
    pointCloud = np.zeros((len(contourCode) + 1, 2), dtype="float32")
    pointCloud[0, 0] = firstPoint[0]
    pointCloud[0, 1] = firstPoint[1]
    for (ndx, value) in enumerate(contourCode):
        pointCloud[ndx + 1, 0] = pointCloud[ndx, 0] + value.real
        pointCloud[ndx + 1, 1] = pointCloud[ndx, 1] + value.imag
    return pointCloud

def convertContourCodeToMsgPointCloud(contourCode, firstScanValue, pose):
    """
    Convert the contour into a pointCloud msg
    Args:
        contourCode (array(complex)) - Difference equivalence code
        firstScanValue (float) - The first value in the laser scan (or radius)
        pose ([float, float, float]) - Pose of point cloud (x, y, theta)
    Returns:
        PointCloud - pointCloud msg
    """
    pointCloud = PointCloud()
    pointCloud.header.stamp = rospy.Time.now()
    pointCloud.header.frame_id = "/odom"
    currentPoint = [firstScanValue, 0]
    rotatedPoint = smath.rotate2D(currentPoint, pose[2])
    pointCloud.points.append(Point32(rotatedPoint[0], rotatedPoint[1], 0))
    for value in contourCode:
        currentPoint[0] += value.real
        currentPoint[1] += value.imag
        rotatedPoint = smath.rotate2D(currentPoint, pose[2])
        pointCloud.points.append(Point32(rotatedPoint[0] + pose[0], rotatedPoint[1] + pose[1], 0))
    return pointCloud

def __getLengthOfTheSegments(contour, approximateLengthSegments):
    """
    Calculates the length by which to divide the contour without residue
    Args:
        contour (list(complex)) - The contour is represented in the form of standard elementary vectors
        approximateLengthSegments (float) - Approximate length of segments
    Returns:
        (float) - length of the segments to which the contour is divided
    """
    length = sum([abs(value) for value in contour])
    countSegments = int(round(length / approximateLengthSegments))
    return length / countSegments, countSegments