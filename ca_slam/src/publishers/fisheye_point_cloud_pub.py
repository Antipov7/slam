#!/usr/bin/env python

"""
Module containing fisheye point cloud publisher
"""

import rospy
import math
import numpy as np
import cv2 as cv
from sensor_msgs.msg import LaserScan, PointCloud2, Image
from nav_msgs.msg import OccupancyGrid
from geometry_msgs.msg import Pose
from cv_bridge import CvBridge
from image_processing import image_processing

class FisheyePointCloudPub():
    """
    """
    def __init__(self):
        self.__bridge = CvBridge()
        self.__pointsPub = rospy.Publisher('/cloud', PointCloud2, queue_size=1)
        self.__imagePub = rospy.Publisher('/ca_slam/panorama_disparity', Image, queue_size=10)
        self.__lastImg = None

    def publish(self, img, R, T):
        """
        Args:
            img (image) - fisheye image
            R (array(3 x 3)) - rotation matrix
            T (array(3 x 1)) - transition matrix
        """
        h, w = img.shape[:2]
        img = cv.resize(img, (w / 3, h / 3))
        if (self.__lastImg is None):
            self.__lastImg =  img
            return

        disparity = image_processing.computeDisparity(img, self.__lastImg)
        self.__lastImg = img
        self.__imagePub.publish(self.__bridge.cv2_to_imgmsg(cv.cvtColor(disparity,cv.COLOR_GRAY2RGB), 'bgr8'))