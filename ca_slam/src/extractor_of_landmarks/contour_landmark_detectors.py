#!/usr/bin/env python

"""
Module that contains various landmark detectors using contour analysis
"""
 
import math
import numpy as np
from toolbox import smath
from abc import ABCMeta, abstractmethod
from scipy.signal import argrelextrema
from landmarks import CornerLandmark, LineLandmark
from contour_analisys import contour_filtering
from toolbox import rosfun
import rospy

def initCornerDetectorUsingRospy():
    """
    Corner detector initialization
    """
    detector = None
    nameDetector = rosfun.fetchParam("~cornerDetector", "matchedFiltering")
    if (nameDetector == "matchedFiltering"):
        windowSize = rosfun.fetchParam("~matchedFilteringSize", 6)
        threshold = rosfun.fetchParam("~matchedFilteringThreshold", 0.6)
        detector = CornerMatchedFilteringDetector([math.pi / 2, -math.pi / 2], windowSize, threshold)
    elif (nameDetector == "curvatureWu"):
        windowSize = rosfun.fetchParam("~curvatureWuSize", 12)
        threshold = rosfun.fetchParam("~curvatureWuThreshold", 0.25)
        detector = CurvatureDetectorWu(windowSize, threshold)
    elif (nameDetector == "curvatureTehChin"):
        windowSize = rosfun.fetchParam("~curvatureTehChinSize", 10)
        threshold = rosfun.fetchParam("~curvatureTehChinThreshold", 0.25)
        detector = CurvatureDetectorTehChin(windowSize, threshold)
    elif (nameDetector == "multiscaleDominant"):
        windowSize = rosfun.fetchParam("~multiscaleDominantSize", 6)
        sigma = rosfun.fetchParam("~multiscaleDominantSigma", 2.0)
        detector = MultiscaleDominantPointDetector(windowSize, sigma)
    elif (nameDetector == "none"):
        detector = EmptyDetector()
    else:
        raise ValueError("Corner detector {0} not found".format(nameDetector))
    return detector

def initStraightLineDetectorUsingRospy():
    """
    Straight line detector initialization
    """
    detector = None
    nameDetector = rosfun.fetchParam("~straightLineDetector", "matchedFiltering")
    if (nameDetector == "matchedFiltering"):
        detector = LineMatchedFilteringDetector()
    elif (nameDetector == "none"):
        detector = EmptyDetector()
    else:
        raise ValueError("Straight line detector {0} not found".format(nameDetector))
    return detector

class CLandmarksDetector():
    """
    Abstract landmark detection class
    """
    __metaclass__  = ABCMeta

    @abstractmethod
    def detect(self, contourFragments, robotPosition):
        """
        Detects landmarks
        Args:
            contourFragments (list(ContourFragment)) - contour fragments
            robotPosition ([float, float, float]) - x, y, theta position of robot
        """
        pass

class CCornerLandmarkDetector(CLandmarksDetector):
    """
    Abstract corner landmark detection class
    """
    pass

class CornerMatchedFilteringDetector(CCornerLandmarkDetector):
    """
    A detector that detects local maxima after matched filtering
    """

    def __init__(self, angles, windowSize = 6, threshold = 0.6):
        """
        Args:
            angles (list[float]) - list of angles to be detected
            windowSize (int) - filter window size. Window size must be an even number
            threshold (float) - threshold at which the detection occurs. Must be in the range of 0 to 1
        """
        super(CornerMatchedFilteringDetector, self).__init__()
        self.__angles = angles
        self.__windowSize = windowSize
        self.__threshold = threshold

    def detect(self, contourFragments, robotPosition):
        """
        Detects landmarks
        Args:
            contourFragments (list(ContourFragment)) - contour fragments
            robotPosition ([float, float, float]) - x, y, theta position of robot
        Returns:
            list(CornerLandmark) - corner landmarks 
        """
        cornerLandmarks = []
        outputFragments = []
        outputNdxs = []
        for angle in self.__angles:
            for contourFragment in contourFragments:
                if (len(contourFragment.contour) <= self.__windowSize):
                    continue

                filterResponse = contour_filtering.selectAngularFragments(contourFragment.contour, angle, self.__windowSize, False)
                thresholdResponse = np.clip(filterResponse, self.__threshold, 1.0)
                localMaxims = np.array([argrelextrema(thresholdResponse, np.greater)][0], 'int32')
                for localMaxima in localMaxims[0]:
                    cornerLandmarks.append(CornerLandmark(contourFragment.pointCloud[localMaxima], robotPosition))
                    outputFragments.append(contourFragment)
                    outputNdxs.append(localMaxima)
        
        return cornerLandmarks, outputFragments, outputNdxs

class CurvatureDetectorWu(CCornerLandmarkDetector):
    """
    A detector that finds points with a local maximum curvature using the Wu method
    """
    def __init__(self, windowSize, threshold = 0.25):
        """
        Args:
            windowSize (int) - window size
            threshold (float) - threshold at which the detection occurs. Must be in the range of 0 to 2
        """
        super(CurvatureDetectorWu, self).__init__()
        if (windowSize % 2 == 1):
            raise ValueError("windowSize must must be an even number")

        self.__windowSize = windowSize
        self.__threshold = threshold

    def detect(self, contourFragments, robotPosition):
        """
        Detects landmarks
        Args:
            contourFragments (list(ContourFragment)) - contour fragments
            robotPosition ([float, float, float]) - x, y, theta position of robot
        Returns:
            list(CornerLandmark) - corner landmarks 
        """
        cornerLandmarks = []
        outputFragments = []
        outputNdxs = []
        startNdx = self.__windowSize // 2
        for contourFragment in contourFragments:
            if (len(contourFragment.pointCloud) <= self.__windowSize):
                continue
            kCosineResponse = np.zeros((len(contourFragment.pointCloud), 1), dtype="float32")
            for i in range(startNdx, len(contourFragment.pointCloud) - startNdx):
                kCosines = self.__getAllKCosineInRegion(i, contourFragment.pointCloud)
                ndxMax = np.argmax(kCosines) + 1
                kCosineResponse[i] = np.sum(kCosines[:ndxMax]) / ndxMax
            thresholdResponse = np.clip(kCosineResponse, self.__threshold, 2.0)
            localMaxims = np.array([argrelextrema(thresholdResponse, np.greater, mode='wrap')][0], 'int32')
            for localMaxima in localMaxims[0]:
                cornerLandmarks.append(CornerLandmark(contourFragment.pointCloud[localMaxima], robotPosition))
                outputFragments.append(contourFragment)
                outputNdxs.append(localMaxima)

        return cornerLandmarks, outputFragments, outputNdxs

    def __getAllKCosineInRegion(self, ndx, pointCloud):
        """
        Returns array of k-cosine values calculated in a region with a window size
        Args:
            ndx (ind) - index of current point in point cloud
            pointCloud (array([float ,float])) - point cloud
        Returns:
            (array(float)) - array of k-cosine values
        """
        kCosines = np.zeros((self.__windowSize // 2, 1), dtype = "float32")
        for i in range(1, self.__windowSize // 2):
            firstVector = pointCloud[ndx] - pointCloud[ndx - i]
            secondVector = pointCloud[ndx] - pointCloud[ndx + i]
            kCosines[i] = self.__kCosineBetweenVectors(firstVector, secondVector)

        return kCosines

    def __kCosineBetweenVectors(self, firstVector, secondVector):
        """
        Determine the k-cosine between two vectors
        Args:
            firstVector ([float, float]) - first vector
            secondVector ([float, float]) - second vector
        Returns:
            (float) - k-cosine of two vectors
        """
        return np.dot(firstVector, secondVector) / (np.linalg.norm(firstVector) * np.linalg.norm(secondVector)) + 1.0

class CurvatureDetectorTehChin(CCornerLandmarkDetector):
    """
    A detector that finds points with a local maximum curvature using the Teh Chin method
    """

    def __init__(self, minContourSize = 10, threshold = 0.25):
        """
        Args:
            minContourSize (int) - minimum contour size that will be processed by the algorithm
            threshold (float) - threshold at which the detection occurs
        """
        super(CurvatureDetectorTehChin, self).__init__()
        self.__threshold = threshold
        self.__minContourSize = minContourSize

    def detect(self, contourFragments, robotPosition):
        """
        Detects landmarks
        Args:
            contourFragments (list(ContourFragment)) - contour fragments
            robotPosition ([float, float, float]) - x, y, theta position of robot
        Returns:
            list(CornerLandmark) - corner landmarks 
        """
        cornerLandmarks = []
        outputFragments = []
        outputNdxs = []
        startNdx = self.__minContourSize // 2
        for contourFragment in contourFragments:
            if (len(contourFragment.pointCloud) <= self.__minContourSize):
                continue
            kCosineResponse = np.zeros((len(contourFragment.pointCloud), 1), dtype="float32")
            for i in range(startNdx, len(contourFragment.pointCloud) - startNdx):
               previousHeight, previousRatio, k = 0.0, 0.0, 1
               height, ratio = self.__getHeightAndRatioDeviationAndChordLength(i, k, contourFragment.pointCloud)
               while (previousHeight < height or previousRatio < ratio) and i - (k + 1) >= 0 and i + (k + 1) < len(contourFragment.pointCloud):
                   k += 1
                   previousHeight, previousRatio = height, ratio
                   height, ratio = self.__getHeightAndRatioDeviationAndChordLength(i, k, contourFragment.pointCloud)
               kCosineResponse[i] = self.__kCosine(i, k, contourFragment.pointCloud)
            thresholdResponse = np.clip(kCosineResponse, self.__threshold, 2.0)
            localMaxims = np.array([argrelextrema(thresholdResponse, np.greater, mode='wrap')][0], 'int32')
            for localMaxima in localMaxims[0]:
                cornerLandmarks.append(CornerLandmark(contourFragment.pointCloud[localMaxima], robotPosition))
                outputFragments.append(contourFragment)
                outputNdxs.append(localMaxima)

        return cornerLandmarks, outputFragments, outputNdxs

    def __getHeightAndRatioDeviationAndChordLength(self, ndx, k, pointCloud):
        """
        Determine ratio of the deviation and the chord length
        Args:
            ndx (int) - index of current point in point cloud
            k (int) - size of support area
            pointCloud (array([float, float]) - point cloud
        Returns:
            (float), (float) - ratio, height
        """
        firstVector = pointCloud[ndx] - pointCloud[ndx - k]
        secondVector = pointCloud[ndx] - pointCloud[ndx + k]
        chordLength = np.linalg.norm(firstVector - secondVector)
        height = self.__getHeightOfTriangle(np.linalg.norm(firstVector), np.linalg.norm(secondVector), chordLength)
        ratio = height / chordLength
        return ratio, height

    def __getHeightOfTriangle(self, a, b, c):
        """
        Returns height of the triangle lowered to the side a
        Args:
            a (float) - side of triangle a
            b (float) - side of triangle b
            c (float) - side of triangle c
        Returns:
            (float) - height of the triangle
        """
        p = 0.5 * (a + b + c)
        d = p * (p -a) * (p - b) * (p - c)
        if (d <= 0.0):
            return 0.0            
        h = 2.0 * math.sqrt(d) / a
        return h

    def __kCosine(self, ndx, k, pointCloud):
        """
        Determine the k-cosine between two vectors
        Args:
            firstVector ([float, float]) - first vector
            secondVector ([float, float]) - second vector
        Returns:
            (float) - k-cosine of two vectors
        """
        firstVector = pointCloud[ndx] - pointCloud[ndx - k]
        secondVector = pointCloud[ndx] - pointCloud[ndx + k]
        return np.dot(firstVector, secondVector) / (np.linalg.norm(firstVector) * np.linalg.norm(secondVector)) + 1.0

class MultiscaleDominantPointDetector(CCornerLandmarkDetector):
    """
    Detector using Scale-space theory
    """

    def __init__(self, size = 12, sigma = 2.0):
        """
        Args:
            size (int) - aperture size
            sigma (float) - standard deviation
        """
        super(MultiscaleDominantPointDetector, self).__init__()
        self.__threshold = 0.05
        self.__windowSize = size
        self.__gaussianKernel = self.__createGaussianKernel(size, sigma)
        self.__firstDerivativeGaussianKernel = self.__createFirstDerivativeGaussianKernel(size, sigma)
        self.__secondDerivativeGaussianKernel = self.__createSecondDerivativeGaussianKernel(size, sigma)

    def detect(self, contourFragments, robotPosition):
        """
        Detects landmarks
        Args:
            contourFragments (list(ContourFragment)) - contour fragments
            robotPosition ([float, float, float]) - x, y, theta position of robot
        Returns:
            list(CornerLandmark) - corner landmarks 
        """
        cornerLandmarks = []
        outputFragments = []
        outputNdxs = []
        for contourFragment in contourFragments:
            if (len(contourFragment.pointCloud) <= self.__windowSize):
                continue
            curvature = self.__calculateCurvatureValues(contourFragment.pointCloud)
            thresholdResponse = np.clip(curvature, self.__threshold, None)
            localMaxims = np.array([argrelextrema(thresholdResponse, np.greater, mode='wrap')][0], 'int32')
            for localMaxima in localMaxims[0]:
                cornerLandmarks.append(CornerLandmark(contourFragment.pointCloud[localMaxima], robotPosition))
                outputFragments.append(contourFragment)
                outputNdxs.append(localMaxima)

        return cornerLandmarks, outputFragments, outputNdxs

    def __calculateCurvatureValues(self, pointCloud):
        """
        Args:
            pointCloud (array([float, float])) - point cloud
        """
        points = np.hsplit(pointCloud, 2)
        pointsX = points[0].ravel()
        pointsY = points[1].ravel()
        curvature = np.zeros((pointCloud.shape[0], 1), dtype="float32")
        firstDerivativeX = np.convolve(pointsX, self.__firstDerivativeGaussianKernel, mode='same')
        secondDerivativeX = np.convolve(pointsX, self.__secondDerivativeGaussianKernel, mode='same')
        firstDerivativeY = np.convolve(pointsY, self.__firstDerivativeGaussianKernel, mode='same')
        secondDerivativeY = np.convolve(pointsY, self.__secondDerivativeGaussianKernel, mode='same')
        for i in range(len(firstDerivativeX)):
            curvature[i] = abs((firstDerivativeX[i] * secondDerivativeY[i] - firstDerivativeY[i] * secondDerivativeX[i])) / math.pow(math.pow(firstDerivativeX[i], 2) + math.pow(firstDerivativeY[i], 2), 3.0 / 2.0)

        return curvature

    def __createGaussianKernel(self, size, sigma):
        """
        Returns Gaussian filter coefficients
        Args:
            size (int) - aperture size
            sigma (float) - gaussian standart deviation
        Returns:
            (array(float)) - gaussian kernel
        """
        kernel = np.zeros((size, 1), dtype="float32").ravel()
        m = size / 2.0
        for i in range(size):
            kernel[i] = (1.0 / sigma * math.sqrt(2 * math.pi)) * math.exp(- math.pow(i - m, 2) / (2.0 * math.pow(sigma, 2)))
        return kernel / sum(kernel)

    def __createFirstDerivativeGaussianKernel(self, size, sigma):
        """
        Returns first derivative Gaussian filter coefficients
        Args:
            size (int) - aperture size
            sigma (float) - gaussian standart deviation
        Returns:
            (array(float)) - first derivative gaussian kernel
        """
        kernel = self.__createGaussianKernel(size, sigma)
        m = size / 2.0
        for i in range(size):
            kernel[i] = - kernel[i] * (i - m) / math.pow(sigma , 2)
        return kernel / sum(kernel)

    def __createSecondDerivativeGaussianKernel(self, size, sigma):
        """
        Returns second derivative Gaussian filter coefficients
        Args:
            size (int) - aperture size
            sigma (float) - gaussian standart deviation
        Returns:
            (array(float)) - second derivative gaussian kernel
        """
        kernel = self.__createGaussianKernel(size, sigma)
        m = size / 2.0
        for i in range(size):
            kernel[i] = - kernel[i] * (math.pow((i - m) / math.pow(sigma, 2), 2) + 1.0 / math.pow(sigma, 2))
        return kernel / sum(kernel)

class CLineLandmarkDetector(CLandmarksDetector):
    """
    Abstract straight line landmark detection class
    """

    def _getLines(self, pointCloud, ndxs):
        """
        Returns straight lines using point cloud and indexes
        Args:
            pointCloud(array([float, float])) - point cloud
            ndxs (array(float)) - point indices that are roughly related to a straight line
        Returns:
            (array([float, float])) - an array of lines that are represented by r and alpha coordinates
        """
        lines = []
        diffNdxs = np.diff(ndxs)
        splitNdxs = np.where(diffNdxs > 1)[0]
        splitNdxs += 1
        fragmentNdxs = np.split(ndxs, splitNdxs)
        for (i, fragment) in enumerate(fragmentNdxs):
            points = np.take(pointCloud, fragment, axis = 0)
            if (len(points) == 0):
                continue
            lines.append(self.__getPolarLineParametersUsingOLSM(points))
        
        lines = self.__mergeLines(lines)
        return lines

    def __getPolarLineParametersUsingOLSM(self, pointCloud):
        """
        Calculates the polar parameters of a straight line using orthogonal least-squares method
        Args:
            pointCloud (array([float, float])) - point cloud
        Returns:
            ([float, float]) - r and alpha coordinates
        """
        sx, sy, sx2, sxy, sy2 = 0.0, 0.0, 0.0, 0.0, 0.0

        splitPoints = np.hsplit(pointCloud, 2)
        pointsX = splitPoints[0].ravel()
        pointsY = splitPoints[1].ravel()

        sx = sum(pointsX) / len(pointsX)
        sy = sum(pointsY) / len(pointsY)

        for point in pointCloud:
            dx = point[0] - sx
            dy = point[1] - sy
            sx2 += math.pow(dx, 2)
            sy2 += math.pow(dy, 2)
            sxy += dx*dy
    
        alpha = 0.5*math.atan2(-2.0 * sxy, sy2 - sx2)
        r = sx * math.cos(alpha) + sy * math.sin(alpha)
        if (r < 0):
            alpha = smath.addAngles(alpha, math.pi)
        return np.array([abs(r), alpha])

    def __mergeLines(self, lines):
        """
        Combines the lines if they are similar
        Args:
            lines (array([float, float])) - straight line array
        Returns:
            (array([float, float])) - straight line array
        """
        if (len(lines) == 0):
            return lines

        mergedLines = []
        for firstLine in lines:
            line = None
            for secondLine in lines:
                if (abs(firstLine[0] - secondLine[0]) <= 0.025 and abs(firstLine[1] - secondLine[1]) <= 0.025):
                    if (line is None):
                        line = (firstLine + secondLine) / 2.0
                    else:
                        line = (line + secondLine) / 2.0
            if (line is not None):
                mergedLines.append(line)
            else:
                mergedLines.append(firstLine)
        return np.unique(mergedLines, axis=0)

class LineMatchedFilteringDetector(CLineLandmarkDetector):
    """
    A detector that detects lines after matched filtering
    """
    
    def __init__(self, windowSize = 18, threshold = 0.8):
        """
        Args:
            windowSize (int) - window size
            threshold (float) - threshold at which the detection occurs. Must be in the range of 0 to 1
        """
        super(LineMatchedFilteringDetector, self).__init__()
        self.__windowSize = windowSize
        self.__threshold = threshold

    def detect(self, contourFragments, robotPosition):
        """
        Detects landmarks
        Args:
            contourFragments (list(ContourFragment)) - contour fragments
            robotPosition ([float, float, float]) - x, y, theta position of robot
        Returns:
            list(CornerLandmark) - corner landmarks 
        """
        lineLandmarks = []
        outputFragments = []
        outputlines = []
        for contourFragment in contourFragments:
            if (len(contourFragment.contour) <= self.__windowSize):
                continue
            filterResponse = contour_filtering.selectLinearFragments(contourFragment.contour, self.__windowSize, False)
            ndxs = np.where(filterResponse > self.__threshold)[0]
            lines = self._getLines(contourFragment.pointCloud, ndxs)
            for line in lines:
                lineLandmarks.append(LineLandmark(line, robotPosition))
                outputFragments.append(contourFragment)
                outputlines.append(line)
            
        return lineLandmarks, outputFragments, outputlines

class EmptyDetector(CCornerLandmarkDetector, CLineLandmarkDetector):
    """
    Detector that does nothing
    """

    def detect(self, contourFragments, robotPosition):
        """
        Empty method
        """
        return None, None, None