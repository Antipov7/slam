#!/usr/bin/env python

"""
Module containing a class describing a database of landmarks
"""

import rospy
import numpy as np
from landmarks_database import LandmarksDatabase

class SimpleLandmarksDatabase(LandmarksDatabase):
    """
    A simple base of landmarks where the association occurs using epsilon neighborhood
    """
    def __init__(self, epsilonNeighborhood = 1):
        super(SimpleLandmarksDatabase, self).__init__()
        self.__epsilonNeighborhood = epsilonNeighborhood

    def landmarksAssociation(self, landmarks):
        """
        Association landmarks using epsilon neighborhood
        Args:
            landmarks (list(Landmark)) - landmarks
        """
        super(SimpleLandmarksDatabase, self).landmarksAssociation(landmarks)
        for landmark in landmarks:
            minDist = self.__epsilonNeighborhood + 1.0
            isLandmarkFound = False
            bestLandmark = None
            for landmarkDB in self._landmarks:
                dist = landmarkDB.descriptor.match(landmark.descriptor)
                if dist < self.__epsilonNeighborhood and dist < minDist:
                    minDist = dist
                    bestLandmark = landmarkDB
                    isLandmarkFound = True

            if isLandmarkFound == False:
                self._addLandmark(landmark)
            elif (bestLandmark not in self._lastObservedLandmarks):
                bestLandmark.associateWithLandmark(landmark)
                self._lastObservedLandmarks.append(bestLandmark)