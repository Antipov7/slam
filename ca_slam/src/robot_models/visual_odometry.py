#!/usr/bin/env python

"""
Module visual odometry
"""

import math
import rospy
import cv2 as cv
import numpy as np
from image_processing import image_processing
from robot_model import RobotModel
from geometry_msgs.msg import Twist

class VisualOdometry(RobotModel):
    """
    Class describing the kinematic motion of the robot
    """
    def __init__(self):
        super(VisualOdometry, self).__init__()
        self.__twistSub = rospy.Subscriber('cmd_vel', Twist, self.twistCallback, queue_size=10)

        self._wheelRadious = 0.5
        self.__velocity = np.array([0, 0, 0, 0, 0, 0], dtype="float32")
        self._stateTransitionMatrix = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]], dtype="float32")
        self._controlMatrix = np.array([[0, 0], [0, 0], [0, 0]], dtype="float32")
        self._controlCovarianceMatrix = np.zeros((2, 2))
        self._covarianceMatrix = np.diag([0, 0, 0])
        self.__orbMatcher = image_processing.ORBMatcher(20)
        self.__previouseImage = None

    def twistCallback(self, msg):
        """
        Receives and saves the twist message
        Args:
            msg (Twist) - twist message
        """
        self.__velocity[0] = msg.linear.x
        self.__velocity[1] = msg.linear.y
        self.__velocity[2] = msg.linear.z
        self.__velocity[3] = msg.angular.x
        self.__velocity[4] = msg.angular.y
        self.__velocity[5] = -msg.angular.z

    def update(self, sensorData):
        """
        Update position, stateTransitionMatrix, controlMatrix
        Args:
            sendorData (SensorData) - sensor data
        """
        self._sensorData = sensorData
        if self.__previouseImage is None:
            self.__previouseImage = cv.cvtColor(sensorData.getImage(), cv.COLOR_BGR2GRAY)
            self.__previousePanoramicImage = cv.cvtColor(sensorData.getPanoramicImage(), cv.COLOR_BGR2GRAY)
            return
        
        dt = sensorData.getDeltaTime()
        velocity = np.copy(self.__velocity)
        velocity[5] = -velocity[5]

        image = cv.cvtColor(sensorData.getImage(), cv.COLOR_BGR2GRAY)
        panoramicImage = cv.cvtColor(sensorData.getPanoramicImage(), cv.COLOR_BGR2GRAY)

        kp1, kp2 = self.__orbMatcher.findMatches(image, self.__previouseImage)
        motion = image_processing.findPlanarMotions(kp1, kp2, sensorData.getCameraInfo())
        
        self.__previouseImage = image
        self.__previousePanoramicImage = panoramicImage

        dist = 3.0 * math.sqrt(motion[0] * motion[0] + motion[1] * motion[1])
        if (velocity[0] < 0):
            dist = -dist
            motion[2] = -motion[2]

        self._position[0] -= dist * math.sin(self._position[2]) 
        self._position[1] += dist * math.cos(self._position[2])
        self._position[2] -= motion[2] + 0.03 * velocity[5] * sensorData.getDeltaTime()

    def getStatePrediction(self):
        """
        Returns state prediction
        Returns:
            ([float, float, float]) - x, y, theta coordinates
        """
        return self._position      

    def getTransitionMatrix(self):
        """
        Returns transition matrix
        Returns:
            ([[float, float, float], [float, float, float], [float, float, float]]) - Jacobian matrix ([[dstatex / dx, dstatex / dy, dstatex / dtheta], [dstatey / dx, dstatey / dy, dstatetheta / dtheta], [dstatez / dx, dstatez / dy, dstatetheta / dtheta]])
        """
        return self._stateTransitionMatrix

    def getControlMatrix(self):
        """
        Returns control matrix
        Returns:
            ([[float, float], [float, float], [float, float]]) - Jacobian matrix ([[dstatex / dvx, dstatex / dvtheta], [dstatey / dvx, dstatey / dvtheta] ,[dstatetheta / dvx, dstatetheta / dvtheta]])
        """
        return self._controlMatrix

    def getCovarianceMatrix(self):
        """
        Return covariance error matrix
        Returns:
            ([[float, float, float], [float, float, float], [float, float, float]])
        """
        return self._covarianceMatrix

    def getControlCovarianceMatrix(self):
        """
        Return control covariance error matrix
        Returns:
            ([[float, float], [float, float]])
        """
        self._controlCovarianceMatrix[0, 0] = (0.9 * self.__velocity[0]) * (0.9 * self.__velocity[0])
        self._controlCovarianceMatrix[1, 1] = (0.9 * self.__velocity[5]) * (0.9 * self.__velocity[5])
        return self._controlCovarianceMatrix

    def setState(self, state):
        """
        Set state
        Args:
            state ([float, float, float]) - x, y, theta coordinates
        """
        self._position = state

    def setCovarianceMatrix(self, covariance):
        """
        Set covariance
        Args:
            covariance ([[float, float, float], [float, float, float], [float, float, float]]) - covariance matrix
        """
        self._covarianceMatrix = covariance