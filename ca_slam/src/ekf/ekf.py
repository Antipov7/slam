#!/usr/bin/env python

"""
Module contains an extended Kalman filter
"""

import rospy
import ekf_local_map
import numpy as np
import ekf_logs
from ekf_map import EkfMap
from toolbox import rosfun

class Ekf():
    """
    Extended Kalman filter
    """
    def __init__(self, robot):
        """
        Initialization state, covariance and transition matrix of the system. matrixToLandmarksTuples contains a pair of values: position and size of matrix blocks that correspond to landmarks
        Args:
            robot (RobotModel) - model describing robot
        """
        self.__map = EkfMap(robot)
        self.__localMapping = self.__initLocalMapping()
        self.__log = self.__initLog()

    def __initLocalMapping(self):
        """
        Local mapping algorithm initialization
        """
        localMapping = None
        nameLocalMapping =  rosfun.fetchParam("~localMapping", "globalMapping")
        if (nameLocalMapping == "globalMapping"):
            localMapping = ekf_local_map.GlobalMapping()
        elif (nameLocalMapping == "AR"):
            localMapping = ekf_local_map.AdaptiveObservationRange()
        elif (nameLocalMapping == "DAC"):
            localMapping = ekf_local_map.DivideAndConquer()
        else:
            raise ValueError("Local mapping algorithm {0} not found".format(nameLocalMapping))
        return localMapping

    def __initLog(self):
        """
        Ekf log initialization
        """
        log = None
        nameLog = rosfun.fetchParam("~ekfLog", "none")
        if (nameLog == "none"):
            log = ekf_logs.EmptyEkfLog()
        elif (nameLog == "JLog"):
            mapName = rosfun.fetchParam("~mapName", "map_01")
            elapsedTime = rosfun.fetchParam("~elapsedTime", 1.0)
            log = ekf_logs.EkfJLog(mapName, elapsedTime)
        elif (nameLog == "PoseLog"):
            mapName = rosfun.fetchParam("~mapName", "map_01")
            log = ekf_logs.EkfPoseLog(mapName)
        else:
            raise ValueError("Ekf log with name {0} not found".format(nameLog))
        return log

    def update(self, robot, landmarksDatabase, deltaTime):
        """
        Update system state using extended Kalman filter
        Args:
            robot (RobotModel(EkfModelSystem)) - model describing robot
            landmarksDatabase (LandmarksDatabase) - landmarks database
            deltaTime (float) - delta time
        """
        self.__addNewLandmarks(landmarksDatabase)
        localMap, localLandmarks, localObservedLandmarks = self.__localMapping.buildLocalMap(self.__map, robot, landmarksDatabase)
        self.__predictionStep(localMap, robot, localLandmarks)
        self.__correctionStep(localMap, robot, localObservedLandmarks)
        self.__localMapping.mergeLocalMapToGlobalMap(localMap, self.__map)
        self.__updateRobot(self.__map, robot)
        self.__updateLandmarks(self.__map, localObservedLandmarks)
        self.__log.info(self.__map, landmarksDatabase, deltaTime)

    def __addNewLandmarks(self, landmarksDatabase):
        """
        Forms the state, covariance and transition matrix of the system
        Args:
            landmarksDatabase (LandmarksDatabase) - landmarks database
        """
        landmarks = landmarksDatabase.getNewLandmarks()
        if (len(landmarks)) == 0:
            return

        for landmark in landmarks:
            self.__map.addSystemObjectToMap(landmark)

    def __predictionStep(self, ekfMap, robot, landmarks):
        """
        The prediction step of the Kalman filter. Prediction of the system state and prediction of covariance error.
        m(t) = g(m(t-1), u(t))
        c(t) = dg * c(t-1) * dg.T + R
        R(t) = dgc * cc * dgc.T
        where: m(t) - system state at time t; g(m(t-1), u(t)) - current state system prediction function; u(t) - control action on the system;
               c(t) - prediction error at time t; dg - state transition matrix; dg.T - transpose state transition matrix; R - system noise;
               dgc - control transition matrix; cc - covariance matrix of control; dgc.T - transpose control transition matrix
        Args:
            ekfMap (EkfMap) - map
            robot (RobotModel) - model describing robot
            landmarks (Landmarks) - landmarks
        Returns:
            m(t), c(t) - prediction of system state and prediction of covariance
        """
        dg = np.zeros(ekfMap.covariance.shape)
        R = np.zeros(ekfMap.covariance.shape)
        position, size = ekfMap.getPositionMatrixBlockOfSystemObject(robot.getId())
        ekfMap.state[position:position + size] = robot.getStatePrediction()
        dgc = robot.getControlMatrix()
        cc = robot.getControlCovarianceMatrix()
        R[position:size, position:size] = np.dot(dgc, np.dot(cc, dgc.transpose()))

        dg[position:size, position:size] = robot.getTransitionMatrix()
        for landmark in landmarks:
            position, size = ekfMap.getPositionMatrixBlockOfSystemObject(landmark.getId())
            dg[position:position + size, position:position + size] = landmark.getTransitionMatrix()
            
        ekfMap.covariance = np.dot(dg, np.dot(ekfMap.covariance, dg.transpose())) + R

    def __correctionStep(self, ekfMap, robot, landmarks):
        """
        The correction step of the Kalman filter. Update system state and update covariance
        k = cpt * hmt * inv(hm * cpt * hmt + q)
        m(t) = mpt + k * (z - h(mp))
        c(t) = (I - k * hm) * cp(t)
        where: k - Kalman gain; cpt - prediction of covariance error; hm - measurement transition matrix; q - measurement covariance noise
               mpt - prediction of system state; z - current measurement; h(mp) - measurement;
               I - innovation; I = z - h(mp)
        Args:
            ekfMap (EkfMap) - map
            robot (RobotModel) - model describing robot
            landmarks (Landmarks) - observed landmarks
        Returns:
            (array) - system state
            (array) - covariance
        """
        for landmark in landmarks:
            hm = self.__getMeasurementMatrix(robot, landmark, ekfMap)
            q = landmark.getMeasurementCovariance()
            k = np.dot(np.dot(ekfMap.covariance,  hm.transpose()), np.linalg.inv(np.dot(np.dot(hm, ekfMap.covariance), hm.transpose()) + q))
            ekfMap.state = ekfMap.state + np.dot(k, landmark.getInovation(robot))
            ekfMap.covariance = np.dot((np.eye(np.size(ekfMap.state)) - np.dot(k, hm)), ekfMap.covariance)

    def __getMeasurementMatrix(self, robot, landmark, ekfMap):
        """
        Forms a measurement matrix for landmark
        Args:
            robot (RobotModel) - model describing robot
            landmark (EkfLandmarkModel) - landmark
            ekfMpa (EkfMap) - map
        Returns:
            (array) - measurement matrix
        """
        lhm = landmark.getMeasurementMatrix(robot)
        hm = np.zeros((lhm.shape[0], ekfMap.getMapSize()))
        position, robotMatrixSize = ekfMap.getPositionMatrixBlockOfSystemObject(robot.getId())
        hm[:, 0 : robotMatrixSize] = lhm[:, 0 : robotMatrixSize]
        position, size = ekfMap.getPositionMatrixBlockOfSystemObject(landmark.getId())
        size = lhm.shape[1] - robotMatrixSize
        hm[:, position : position + size] = lhm[:, robotMatrixSize:]
        return hm

    def __updateLandmarks(self, ekfMap, landmarks):
        """
        Update observer landmarks. Update state and covariance of the landmarks
        Args:
            ekfMap (EkfMap) - map
            landmarks (list(Landmark)) - observed landmarks
        """
        for landmark in landmarks:
            landmark.setState(ekfMap.getStateOfSystemObject(landmark.getId()))
            landmark.setCovarianceMatrix(ekfMap.getCovarianceOfSystemObject(landmark.getId()))

    def __updateRobot(self, ekfMap, robot):
        """
        Update state and covariance of the robot
        Args:
            ekfMap (ekfMap) = map
            robot (RobotModel) - model describing robot
        """
        robot.setState(ekfMap.getStateOfSystemObject(robot.getId()))
        robot.setCovarianceMatrix(ekfMap.getCovarianceOfSystemObject(robot.getId()))