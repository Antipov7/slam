#!/usr/bin/env python

"""
Module with various contour filters
"""

import rospy
import math
import cmath
import numpy as np

def selectLinearFragments(contourCode, windowSize, isCircular = True):
    """
    Extraction of a linear fragments in the contour.
    Args:
        contourCode (list(complex)) - Difference equivalence code. Contour must be closed
        windowSize (int) - Filter window size. Window size must be an odd number
        isCircular (bool) - Input contour values outside the bounds of the contour are computed by implicitly assuming the input contour is periodic.
    Returns:
        (list(float)) - array between 0 and 1, where 1 is a linear fragment of the contour
    """
    if (len(contourCode) <= windowSize):
        raise ValueError("windowSize must be less than the contour size")

    startNdx = 0
    endNdx = len(contourCode)
    if (isCircular == False):
        startNdx = windowSize // 2
        endNdx = len(contourCode) - startNdx

    result = np.zeros(len(contourCode))
    for ndx in range(startNdx, endNdx):
        window = __getWindow(contourCode, ndx, windowSize)
        squareLength = math.pow(abs(sum(window)), 2)
        squareNorm = sum([math.pow(abs(value), 2) for value in window])
        result[ndx] = squareLength / (squareNorm * windowSize)
    
    return result

def selectAngularFragments(contourCode, angle, windowSize, isCircular = True):
    """
    Extraction of a angular fragments in the contour.
    Args:
        contourCode (list(complex)) - difference equivalence code. Contour must be closed
        angle (float) - distinguished angle (radians)
        windowSize (int) - filter window size. Window size must be an even number
        isCircular (bool) - input contour values outside the bounds of the contour are computed by implicitly assuming the input contour is periodic.
    Returns:
        (list(float)) - array of 0 and 1, where 1 is angular fragment of the contour
    """
    if (len(contourCode) <= windowSize):
        raise ValueError("windowSize must be less than contour size")

    if (windowSize % 2 == 1):
        raise ValueError("windowSize must be an even number")

    startNdx = 0
    endNdx = len(contourCode)
    if (isCircular == False):
        startNdx = windowSize // 2
        endNdx = len(contourCode) - (windowSize - startNdx)

    result = np.zeros(len(contourCode))
    halfWindowSize = windowSize // 2
    c = 0.5 / math.pow(halfWindowSize, 0.5)
    for ndx in range(startNdx, endNdx):
        window = __getWindow(contourCode, ndx, windowSize)
        firstWindow = window[:halfWindowSize]
        secondWindow = __rotateContour(window[halfWindowSize + 1:], angle)
        firstHalfNormalizedResponse = __getHalfNormalizedResponse(firstWindow)
        secondHalfNormalizedResponse = __getHalfNormalizedResponse(secondWindow)
        result[ndx] = c * abs(firstHalfNormalizedResponse - secondHalfNormalizedResponse)
        
    return result

def __getWindow(contourCode, ndx, size):
    """
    Return the sliding window for filtering
    Args:
        contourCode (list(complex)) - difference equivalence code. Contour must be closed
        ndx (int) - index. Ndx is the center of the sliding window
        size (int) - filter window size
    Returns:
        (list(complex)) - filter window ndx:ndx+size 
    """
    startWindow = ndx - size // 2
    endWindow = ndx + size // 2 + 1
    if ((startWindow >= 0) and (endWindow <= len(contourCode))):
        return contourCode[startWindow:endWindow]
    elif startWindow < 0:
        return np.concatenate((contourCode[startWindow:], contourCode[:endWindow]))
    else:
        return np.concatenate((contourCode[startWindow:], contourCode[:endWindow - len(contourCode)]))

def __getHalfNormalizedResponse(contourCode):
    """
    Return half normalized response
    Args:
        contourCode (list(complex)) - difference equivalence code
    Returns:
        (float) - half normalized response
    """
    a = sum(contourCode)
    b = math.pow(sum([math.pow(abs(value), 2) for value in contourCode]), 0.5)
    return a / b

def __rotateContour(contourCode, angle):
    """
    Rotates the contour by a given angle
    Args:
        contourCode (list(complex)) - difference code
        angle (float) - angle of rotation (radians)
    Returns:
        (list(complex)) - difference code
    """
    for i in range(len(contourCode)):
        contourCode[i] = -contourCode[i] * cmath.exp(-1j * angle)
    return contourCode