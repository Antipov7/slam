#!/usr/bin/env python

"""
Module containing landmarks
"""

import rospy
import math
import numpy as np
from toolbox import smath
from abc import ABCMeta, abstractmethod
from image_processing import image_processing
from ekf.ekf_system_model import EkfLandmarkModel, EkfSystemModel

class Landmark(EkfSystemModel, EkfLandmarkModel):
    """
    Abstract class landmarks
    """
    __metaclass__ = ABCMeta
    
    def __init__(self):
        """
        Initialize landmark
        """
        super(Landmark, self).__init__()
        self._globalPosition = np.zeros((2, 1), dtype=float)
        self.descriptor = None

    def associateWithLandmark(self, landmark):
        """
        Copy current measurement
        """
        self._currentMeasurement = landmark.getCurrentMeasurement()
        
    def getGlobalPosition(self):
        """
        Returns global position
        Returns:
            ([float, float]) - x, y global coordinates
        """
        return self._globalPosition

    @abstractmethod
    def getType(self):
        """
        Returns type of landmark
        Returns:
            (string) - type of landmark
        """
        pass

class CornerLandmark(Landmark):
    """
    Landmark type corner
    """
    def __init__(self, localPosition, robotPosition):
        """
        Args:
            localPosition ([float, float]) - position of landmark
            robotPosition ([float, float, float]) - x, y, theta position of robot
        """
        super(CornerLandmark, self).__init__()

        transposeMatrix = np.array([robotPosition[0], robotPosition[1]])
        self._globalPosition = smath.rotate2D(localPosition, -robotPosition[2]) + transposeMatrix

        r = np.linalg.norm(localPosition)
        alpha = math.atan2(localPosition[1], localPosition[0])
        self._currentMeasurement = np.array([r, alpha])

        self.__measurementCovariance = np.diag([math.pow(5.0, 2) , math.pow(15.0 / 180.0 * math.pi, 2)])

        self.__transitionMatrix = np.eye(2, dtype="float32")
        self.__covarianceMatrix = np.array([[1.0, 0], [0, 1.0]], dtype="float32")

    def getInovation(self, robot):
        """
        Returns inovation. Difference between current measurement and measurement
        Args:
            robot (RobotModel) - robot model
        """
        robotPosition = robot.getPosition()
        dx = self._globalPosition[0] - robotPosition[0]
        dy = self._globalPosition[1] - robotPosition[1]
        r = math.sqrt(math.pow(dx, 2) + math.pow(dy, 2))
        alpha = smath.subAngles(math.atan2(dy, dx), robotPosition[2])
        innovation = self._currentMeasurement - np.array([r, alpha])
        innovation[1] = smath.clampAngle(innovation[1])
        return innovation

    def getCurrentMeasurement(self):
        """
        Returns current measurement
        Returns:
            ([float, float]) - r, alpha
        """
        return self._currentMeasurement

    def getMeasurement(self, robot):
        """
        Returns measurement
        Args:
            robot (RobotModel) - robot model
        Returns:
            ([float, float]) - r, alpha measurements
        """
        robotPosition = robot.getPosition()
        dx = self._globalPosition[0] - robotPosition[0]
        dy = self._globalPosition[1] - robotPosition[1]
        r = math.sqrt(math.pow(dx, 2) + math.pow(dy, 2))
        alpha = smath.subAngles(math.atan2(dy, dx), robotPosition[2])
        return np.array([r, alpha])
    
    def getMeasurementMatrix(self, robot):
        """
        Returns relationship matrix of measurement and state
        Args:
            robot (RobotModel) - robot model
        Returns:
            ([[float, float, float, float, float], [float, float, float, float, float]]) - jacobian matrix of measurement ([[dr/dx, dr/dy, dr/dalpha, dr/dxm, dr/dym], [dalpha/dx, dalpha/dy, dalpha/dalpha, dalpha/dxm, dalpha/dym]])
        """
        robotPosition = robot.getPosition()
        dx = self._globalPosition[0] - robotPosition[0]
        dy = self._globalPosition[1] - robotPosition[1]
        q = math.pow(dx, 2) + math.pow(dy, 2)
        return np.array([[-dx / math.sqrt(q), -dy / math.sqrt(q), 0, dx / math.sqrt(q), dy / math.sqrt(q)], 
                         [dy / q, -dx / q, -1, -dy / q, dx / q]], dtype='float32')
    
    def getMeasurementCovariance(self):
        """
        Returns measurement noise covariance
        Returns:
            ([[float, float], [float, float]]) - measurement noise covariance
        """
        return self.__measurementCovariance

    def getStatePrediction(self):
        """
        Returns prediction state
        Returns:
            ([float, float]) - x, y coordinates
        """
        return self._globalPosition

    def getCovarianceMatrix(self):
        """
        Returns covariance error matrix
        Returns:
            ([[float, float] , [float, float]]) - covariance error matrix
        """
        return self.__covarianceMatrix

    def getTransitionMatrix(self):
        """
        Returns Jacobian matrix of state
        Returns:
            ([[float, float], [float, float]]) - Jacobian matrix ([[dstatex / dx, dstatex / dy], [dstatey / dx, dstatey / dy]])
        """
        return self.__transitionMatrix

    def setState(self, state):
        """
        Set state 
        """
        self._globalPosition = state

    def setCovarianceMatrix(self, covariance):
        """
        Set covariance matrix
        """
        self.__covarianceMatrix = covariance

    def getType(self):
        """
        Returns type of landmark
        Returns:
            (string) - type of landmark
        """
        return "corner"

class LineLandmark(Landmark):
    """
    Landmark type line
    """
    def __init__(self, polarLineParameters, robotPosition):
        """
        Args:
            polarLineParameters ([float, float]) - r, alpha coordinates
            robotPosition ([float, float, float]) - x, y, theta position of robot
        """
        super(LineLandmark, self).__init__()

        center = smath.rotate2D(robotPosition, robotPosition[2])
        self.__linePosition = smath.straightLinePolarTranslation(polarLineParameters[0], polarLineParameters[1], center[0], center[1], robotPosition[2])

        self._globalPosition = smath.convertPolarToCartesian(self.__linePosition[0], self.__linePosition[1])

        self._currentMeasurement = polarLineParameters

        self.__measurementCovariance = np.diag([math.pow(5.0, 2) , math.pow(5.0 / 180.0 * math.pi, 2)])

        self.__transitionMatrix = np.eye(2, dtype="float32")
        self.__covarianceMatrix = np.array([[25, 0], [0, math.pow(5.0 / 180.0 * math.pi, 2)]], dtype="float32")

    def getInovation(self, robot):
        """
        Returns inovation. Difference between current measurement and measurement
        Args:
            robot (RobotModel) - robot model
        """
        robotPosition = robot.getPosition()

        measurement = smath.straightLinePolarTranslation(self.__linePosition[0], self.__linePosition[1], robotPosition[0], robotPosition[1], robotPosition[2])
        inovation = self._currentMeasurement - measurement
        inovation[1] = smath.clampAngle(inovation[1])
        return inovation

    def getCurrentMeasurement(self):
        """
        Returns current measurement
        Returns:
            ([float, float]) - r, alpha
        """
        return self._currentMeasurement
    
    def getMeasurementMatrix(self, robot):
        """
        Returns relationship matrix of measurement and state
        Args:
            robot (RobotModel) - robot model
        Returns:
            ([[float, float, float, float, float], [float, float, float, float, float]]) - jacobian matrix of measurement ([[dr/dx, dr/dy, dr/dalpha, dr/dxm, dr/dym], [dalpha/dx, dalpha/dy, dalpha/dalpha, dalpha/dxm, dalpha/dym]])
        """
        robotPosition = robot.getPosition()
        sinAlpha = math.sin(self._currentMeasurement[1])
        cosAlpha = math.cos(self._currentMeasurement[1])
        return np.array([[-cosAlpha, -sinAlpha, 0, 1, robotPosition[0] * sinAlpha - robotPosition[1] * cosAlpha], 
                         [0, 0, -1, 0, 1]], dtype='float32')
    
    def associateWithLandmark(self, landmark):
        """
        Copy current measurement and global position
        """
        self._currentMeasurement = landmark.getCurrentMeasurement()
        self._globalPosition = landmark.getGlobalPosition()

    def getMeasurementCovariance(self):
        """
        Returns measurement noise covariance
        Returns:
            ([[float, float], [float, float]]) - measurement noise covariance
        """
        return self.__measurementCovariance

    def getStatePrediction(self):
        """
        Returns prediction state
        Returns:
            ([float, float]) - r, alpha coordinates
        """
        return self.__linePosition

    def getCovarianceMatrix(self):
        """
        Returns covariance error matrix
        Returns:
            ([[float, float] , [float, float]]) - covariance error matrix
        """
        return self.__covarianceMatrix

    def getTransitionMatrix(self):
        """
        Returns Jacobian matrix of state
        Returns:
            ([[float, float], [float, float]]) - Jacobian matrix ([[dstater / dr, dstater / dalpha], [dstatealpha / dr, dstatealpha / dalpha]])
        """
        return self.__transitionMatrix

    def setState(self, state):
        """
        Set state 
        """
        self.__linePosition = state

    def setCovarianceMatrix(self, covariance):
        """
        Set covariance matrix
        """
        self.__covarianceMatrix = covariance

    def getType(self):
        """
        Returns type of landmark
        Returns:
            (string) - type of landmark
        """
        return "line"

class FeatureLandmark(Landmark):
    """
    Landmark type feature
    """
    def __init__(self, ray, imagePoint, robot):
        """
        Args:
            ray ([float, float, float]) - ray aimed at a feature from the center of the camera
            imagePoint ([float, float]) - image point coordinates
            robot (RobotModel) - robot
        """
        super(FeatureLandmark, self).__init__()
        self.__initImagePoint = imagePoint
        self.__initCameraPosition = robot.getCameraPosition()
        [r, alpha, phi] = smath.convertCartesianToSpherical(ray[0], ray[1], ray[2])
        self.__inverseDepthParameters = np.array([self.__initCameraPosition[0], self.__initCameraPosition[1], self.__initCameraPosition[2], alpha, phi, 0.1])

        self._currentMeasurement = np.copy(imagePoint)
        self.__measurementCovariance = np.diag([math.pow(1.0, 2), math.pow(1.0, 2)])
        self.__transitionMatrix = np.eye(6, dtype="float32")
        self.__transitionMatrix[0, 0] = 0
        self.__transitionMatrix[1, 1] = 0
        self.__transitionMatrix[2, 2] = 0

        self.__covarianceMatrix = np.eye(6, dtype="float32")

    def associateWithLandmark(self, landmark):
        """
        Copy current measurement
        """
        self._currentMeasurement = landmark.getCurrentMeasurement()

    def __inverseDepthToCartasian(self, robot):
        """
        Converts the coordinates of the inverse depth to Cartesian coordinates
        Args:
            robot (RobotModel) - robot
        """
        robotPosition = robot.getPosition()
        rwc = robot.getCameraPosition()
        rotationMatrix = smath.getRotation3DInPlane(robotPosition[2])
        return np.dot(rotationMatrix, self.__inverseDepthParameters[5] * (self.__initCameraPosition - rwc) + smath.convertSphericalToCartesian(1.0, self.__inverseDepthParameters[3], self.__inverseDepthParameters[4]))

    def getGlobalPosition(self):
        """
        Returns global position
        Returns:
            ([float, float, float]) - x, y, z global coordinates
        """
        self.__globalPosition = self.__initCameraPosition + smath.convertSphericalToCartesian(1.0, self.__inverseDepthParameters[3], self.__inverseDepthParameters[4]) / self.__inverseDepthParameters[5]
        return self.__globalPosition

    def getInovation(self, robot):
        """
        Returns inovation. Difference between current measurement and measurement
        Args:
            robot (RobotModel) - robot model
        """
        cameraInfo = robot.getSensorData().getCameraInfo()
        point = self.__inverseDepthToCartasian(robot)
        measurement = image_processing.world2cam(point, cameraInfo)
        inovation = self._currentMeasurement - measurement
        return inovation

    def getCurrentMeasurement(self):
        """
        Returns current measurement
        Returns:
            ([float, float, float]) - x, y, z
        """
        return self._currentMeasurement

    def getMeasurement(self, robot):
        """
        Returns measurement
        Args:
            robot (RobotModel) - robot model
        Returns:
            ([float, float]) - r, alpha measurements
        """
        cameraInfo = robot.getSensorData().getCameraInfo()
        point = self.__inverseDepthToCartasian(robot)
        measurement = image_processing.world2cam(point, cameraInfo)
        return measurement
    
    def getMeasurementMatrix(self, robot):
        """
        Returns relationship matrix of measurement and state
        Args:
            robot (RobotModel) - robot model
        Returns:
            ([[float, float, float, float, float], [float, float, float, float, float]]) - jacobian matrix of measurement ([[dr/dx, dr/dy, dr/dalpha, dr/dxm, dr/dym], [dalpha/dx, dalpha/dy, dalpha/dalpha, dalpha/dxm, dalpha/dym]])
        """
        robotPosition = robot.getPosition()
        point = self.__inverseDepthToCartasian(robot)
        cameraInfo = robot.getSensorData().getCameraInfo()
        rotationMatrix = smath.getRotation3DInPlane(robotPosition[2])
        rwc = robot.getCameraPosition()
        d = robot.getCameraDistance()
        sin_a = math.sin(robotPosition[2])
        cos_a = math.cos(robotPosition[2])

        dh_dxr = self.__inverseDepthParameters[5] * np.dot(rotationMatrix, np.array([[-1.0], [0.0], [0.0]]))
        dh_dyr = self.__inverseDepthParameters[5] * np.dot(rotationMatrix, np.array([[0.0], [-1.0], [0.0]]))
        dh_dalpha = np.transpose(np.array([np.dot(np.array([[-sin_a, -cos_a, 0.0], [cos_a, -sin_a, 0.0], [0.0, 0.0, 0.0]]), self.getGlobalPosition())])) + self.__inverseDepthParameters[5] * np.dot(rotationMatrix, np.array([[-d * sin_a], [-d * cos_a], [0.0]]))

        p = self.__p(point)
        df_dh = np.array([[self.__du_dx(point, p, cameraInfo), self.__du_dy(point, p, cameraInfo), self.__du_dz(point, p, cameraInfo)], 
                         [self.__dv_dx(point, p, cameraInfo), self.__dv_dy(point, p, cameraInfo), self.__dv_dz(point, p, cameraInfo)]])

        Hxyz = np.array([[self.__inverseDepthParameters[5] * math.cos(robotPosition[2] - self.__inverseDepthParameters[5] * self.__inverseDepthParameters[1] * math.sin(robotPosition[2])), 0.0, 0.0],  
                        [0.0, self.__inverseDepthParameters[5] * self.__inverseDepthParameters[0] * math.sin(robotPosition[2]) + self.__inverseDepthParameters[5] * math.cos(robotPosition[2]), 0.0], 
                        [0.0, 0.0, self.__inverseDepthParameters[5]]])

        Halpha = np.dot(rotationMatrix, np.array([[-math.sin(self.__inverseDepthParameters[4]) * math.sin(self.__inverseDepthParameters[3])], [math.sin(self.__inverseDepthParameters[4]) * math.cos(self.__inverseDepthParameters[3])], [0.0]]))
        Hphi = np.dot(rotationMatrix, np.array([[math.cos(self.__inverseDepthParameters[4]) * math.sin(self.__inverseDepthParameters[3])], [math.cos(self.__inverseDepthParameters[4]) * math.sin(self.__inverseDepthParameters[3])], [-math.sin(self.__inverseDepthParameters[4])]]))
        Hro = np.array([np.dot(rotationMatrix, self.__initCameraPosition - rwc + smath.convertSphericalToCartesian(1.0, self.__inverseDepthParameters[3], self.__inverseDepthParameters[4]))])
        Hro = np.transpose(Hro)
        dm_dh = np.hstack((dh_dxr, dh_dyr, dh_dalpha, Hxyz, Halpha, Hphi, Hro))
        return np.dot(df_dh, dm_dh)

    def __p(self, point):
        """
        Return distance of a point from image center
        Args:
            point ([float, float, float]) - point in space
        Return
            float - distance of a point from image center
        """
        norm = math.sqrt(point[0] * point[0] + point[1] * point[1])
        return math.atan(point[2] / norm)

    def __du_dx(self, point, p, cameraInfo):
        """
        Return du/dx
        Args:
            point ([float, float, float]) - point in space
            p (float) - distance of a point from image center
            cameraInfo - cameraInfo
        Return
            du/dx
        """
        p1, p2 = 0, 0
        x, y, z = point[0], point[1], point[2]
        a, b = cameraInfo.affine[0], cameraInfo.affine[1]

        k1 = a / math.sqrt(x*x + y*y) - x*(a*x +b*y) / math.pow(x*x + y*y, 1.5)
        k2 = (a*x + b*y) / math.sqrt(x*x + y*y)

        for i in range(0, len(cameraInfo.inverseCoef)):
            p1 += cameraInfo.inverseCoef[i] * math.pow(p, i)

        n = math.pow(x*x + y*y, 1.5) * (1 + z*z / (x*x + y*y))
        for i in range(1, len(cameraInfo.inverseCoef)):
            p2 += i * x * z * cameraInfo.inverseCoef[i] * math.pow(p, i-1) / n
        return k1*p1 - k2*p2

    def __du_dy(self, point, p, cameraInfo):
        """
        Return du/dy
        Args:
            point ([float, float, float]) - point in space
            p (float) - distance of a point from image center
            cameraInfo - cameraInfo
        Return
            du/dy
        """
        p1, p2 = 0, 0
        x, y, z = point[0], point[1], point[2]
        a, b = cameraInfo.affine[0], cameraInfo.affine[1]

        k1 = b / math.sqrt(x*x + y*y) - y*(a*x +b*y) / math.pow(x*x + y*y, 1.5)
        k2 = (a*x + b*y) / math.sqrt(x*x + y*y)

        for i in range(0, len(cameraInfo.inverseCoef)):
            p1 += cameraInfo.inverseCoef[i] * math.pow(p, i)

        n = math.pow(x*x + y*y, 1.5) * (1 + z*z / (x*x + y*y))
        for i in range(1, len(cameraInfo.inverseCoef)):
            p2 += i * y * z * cameraInfo.inverseCoef[i] * math.pow(p, i-1) / n
        return k1*p1 - k2*p2

    def __du_dz(self, point, p, cameraInfo):
        """
        Return du/dz
        Args:
            point ([float, float, float]) - point in space
            p (float) - distance of a point from image center
            cameraInfo - cameraInfo
        Return
            du/dz
        """      
        x, y, z = point[0], point[1], point[2]
        a, b = cameraInfo.affine[0], cameraInfo.affine[1]
        k1 = (a*x + b*y) / math.sqrt(x*x + y*y)
        p1 = 0
        n = math.sqrt(x*x + y*y) * (1 + z*z / (x*x + y*y))
        for i in range(1, len(cameraInfo.inverseCoef)):
            p1 += i * cameraInfo.inverseCoef[i] * math.pow(p, i-1) / n
        return k1 * p1

    def __dv_dx(self, point, p, cameraInfo):
        """
        Return dv/dx
        Args:
            point ([float, float, float]) - point in space
            p (float) - distance of a point from image center
            cameraInfo - cameraInfo
        Return
            dv/dx
        """
        p1, p2 = 0, 0
        x, y, z = point[0], point[1], point[2]
        c = cameraInfo.affine[2]

        k1 = c / math.sqrt(x*x + y*y) - x*(c*x + y) / math.pow(x*x + y*y, 1.5)
        k2 = (c*x + y) / math.sqrt(x*x + y*y)

        for i in range(0, len(cameraInfo.inverseCoef)):
            p1 += cameraInfo.inverseCoef[i] * math.pow(p, i)

        n = math.pow(x*x + y*y, 1.5) * (1 + z*z / (x*x + y*y))
        for i in range(1, len(cameraInfo.inverseCoef)):
            p2 += i * x * z * cameraInfo.inverseCoef[i] * math.pow(p, i-1) / n
        return k1*p1 - k2*p2

    def __dv_dy(self, point, p, cameraInfo):
        """
        Return dv/dy
        Args:
            point ([float, float, float]) - point in space
            p (float) - distance of a point from image center
            cameraInfo - cameraInfo
        Return
            dv/dy
        """
        p1, p2 = 0, 0
        x, y, z = point[0], point[1], point[2]
        c = cameraInfo.affine[2]

        k1 = 1 / math.sqrt(x*x + y*y) - y*(c*x + y) / math.pow(x*x + y*y, 1.5)
        k2 = (c*x + y) / math.sqrt(x*x + y*y)

        for i in range(0, len(cameraInfo.inverseCoef)):
            p1 += cameraInfo.inverseCoef[i] * math.pow(p, i)

        n = math.pow(x*x + y*y, 1.5) * (1 + z*z / (x*x + y*y))
        for i in range(1, len(cameraInfo.inverseCoef)):
            p2 += i * y * z * cameraInfo.inverseCoef[i] * math.pow(p, i-1) / n
        return k1*p1 - k2*p2

    def __dv_dz(self, point, p, cameraInfo):
        """
        Return dv/dz
        Args:
            point ([float, float, float]) - point in space
            p (float) - distance of a point from image center
            cameraInfo - cameraInfo
        Return
            dv/dz
        """      
        x, y, z = point[0], point[1], point[2]
        c = cameraInfo.affine[2]
        k1 = (c*x + y) / math.sqrt(x*x + y*y)
        p1 = 0
        n = math.sqrt(x*x + y*y) * (1 + z*z / (x*x + y*y))
        for i in range(1, len(cameraInfo.inverseCoef)):
            p1 += i * cameraInfo.inverseCoef[i] * math.pow(p, i-1) / n
        return k1 * p1      

    def getMeasurementCovariance(self):
        """
        Returns measurement noise covariance
        Returns:
            ([[float, float], [float, float]]) - measurement noise covariance
        """
        return self.__measurementCovariance

    def getStatePrediction(self):
        """
        Returns prediction state
        Returns:
            ([float, float, float, float, float, float]) - x, y, z, alpha, phi, ro coordinates
        """
        return self.__inverseDepthParameters

    def getCovarianceMatrix(self):
        """
        Returns covariance error matrix
        Returns:
            (array) - covariance error matrix
        """
        return self.__covarianceMatrix

    def getTransitionMatrix(self):
        """
        Returns Jacobian matrix of state
        Returns:
            (array) - Jacobian matrix
        """
        return self.__transitionMatrix

    def setState(self, state):
        """
        Set state 
        """
        self.__inverseDepthParameters = state

    def setCovarianceMatrix(self, covariance):
        """
        Set covariance matrix
        """
        self.__covarianceMatrix = covariance

    def getType(self):
        """
        Returns type of landmark
        Returns:
            (string) - type of landmark
        """
        return "feature"  