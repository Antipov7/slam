#!/usr/bin/env python

"""
Module contains an extended Kalman filter
"""

from abc import ABCMeta, abstractmethod

class EkfSystemModel():
    """
    Abstract class describing dynamic system model
    """

    def __init__(self):
        self._id = 0

    def getId(self):
        """
        Returns:
            (int) - id
        """
        return self._id
    
    def setId(self, id):
        """
        Args:
            id (int) - id
        """
        self._id = id

    @abstractmethod
    def getStatePrediction(self):
        """
        Returns prediction state
        """
        pass

    @abstractmethod
    def getCovarianceMatrix(self):
        """
        Returns covariance error matrix
        """
        pass

    @abstractmethod
    def getTransitionMatrix(self):
        """
        Returns Jacobian matrix of state
        """
        pass

    @abstractmethod
    def setState(self, state):
        """
        Set state 
        """
        pass

    @abstractmethod
    def setCovarianceMatrix(self, covariance):
        """
        Set covariance matrix
        """
        pass

class EkfControlAction():
    """
    Abstract class describing control action
    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def getControlMatrix(self):
        """
        Returns Jacobian matrix of control
        """
        pass

    @abstractmethod
    def getControlCovarianceMatrix(self):
        """
        Returns control covariance error matrix
        """
        pass

class EkfLandmarkModel():
    """
    Abstract class describing landmark model
    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def getInovation(self, robot):
        """
        Returns inovation. difference between current measurement and measurement
        Args:
            robot (RobotModel) - robot model
        """
        pass

    @abstractmethod
    def getMeasurementMatrix(self, robot):
        """
        Returns relationship matrix of measurement and state
        Args:
            robot (RobotModel) - robot model
        """
        pass

    @abstractmethod
    def getMeasurementCovariance(self):
        """
        Returns measurement noise covariance
        """
        pass