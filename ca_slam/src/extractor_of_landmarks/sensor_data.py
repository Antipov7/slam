#!/usr/bin/env python

"""
Module that contains classes that receive data from sensors and store them
"""

import rospy
import message_filters
from image_processing import image_processing
import cv2 as cv
import numpy as np
from toolbox import sio
from abc import ABCMeta, abstractmethod
from sensor_msgs.msg import CompressedImage, LaserScan
from cv_bridge import CvBridge, CvBridgeError

class SensorData():
    """
    Abstract class that receive data from sensors and store them
    """
    __metaclass__ = ABCMeta

    def __init__(self, callback):
        """
        Args:
            callback (function) - callback function, which is called when data came from sensors 
        """
        self._callback = callback
        self._dt = 0.0

    def getDeltaTime(self):
        """
        Returns the time interval between the last two data received from the sensors
        """
        return self._dt

class LaserScanData(SensorData):
    """
    Class that receive data from laser scan system and store them
    """
    def __init__(self, callback):
        """
        Args:
            callback (function) - callback function, which is called when data came from laser scan system
        """
        super(LaserScanData, self).__init__(callback)
        self.__laserSub = rospy.Subscriber('/scan', LaserScan, self.__msgCallback, queue_size=10)
        self.__lastTime = None
        rospy.spin()

    def getValue(self, ndx):
        """
        Args:
            ndx (int) - value index
        Returns:
            float - lidar value
        """
        return self.__laserScan[ndx]

    def valueIsDefined(self, ndx):
        """
        Args:
            ndx (int) - value index
        Returns:
            bool - returns false if lidar reading is not certain
        """
        return self.__laserScan[ndx] > 0.01

    def getLaserScan(self):
        """
        Returns:
            (LaserScan) - laser scan message
        """
        return self.__laserScan

    def __msgCallback(self, msg):
        """
        Receives and savess the laser scan message
        Args:
            msg (LaserScan) - laser scan message
        """
        if self.__lastTime is not None:
            self._dt = (msg.header.stamp - self.__lastTime).to_sec()
        self.__lastTime = msg.header.stamp
        self.__laserScan = msg
        self._callback(self)

class LaserScanAndImageData(SensorData):
    """
    Class that receive data from laser scan system and image from camera and store them 
    """
    def __init__(self, callback, cameraInfo):
        """
        Args:
            callback (function) - callback function, which is called when data came from laser scan system and camera
        """
        super(LaserScanAndImageData, self).__init__(callback)
        self.__bridge = CvBridge()

        self.__cameraInfo = cameraInfo
        self.__mapX, self.__mapY = image_processing.initFisheyePanoramaMap(self.__cameraInfo, 5.0, (self.__cameraInfo.size[0] / 2, self.__cameraInfo.size[1]))

        self.__imageSub = message_filters.Subscriber('/raspicam_node/image/compressed', CompressedImage)
        self.__laserSub = rospy.Subscriber('/scan', LaserScan, self.__msgsCallback, queue_size=1)
        self.__imgCache = message_filters.Cache(self.__imageSub, 3)
        self.__lastTime = None
        rospy.spin()

    def getLaserScan(self):
        """
        Returns:
            (LaserScan) - laser scan message
        """
        return self.__laserScan

    def getImage(self):
        """
        Returns:
            (Cv_image) - color image with rgb8
        """
        return self.__image

    def getPanoramicImage(self):
        """
        Returns:
            (Cv_image) - panoramic image
        """
        return self.__panoramicImage

    def getCameraInfo(self):
        """
        Returns:
            (OcamInfo) - camera info
        """
        return self.__cameraInfo

    def __msgsCallback(self, scan):
        """
        Args:
            image (Image) - image message
            scan (LaserScan) - laser scan message
        """
        if self.__lastTime is not None:
            self._dt = (scan.header.stamp - self.__lastTime).to_sec()
        self.__lastTime = scan.header.stamp
        stamp = self.__imgCache.getLatestTime()
        image = self.__imgCache.getElemBeforeTime(stamp)
        try:
            self.__image = self.__bridge.compressed_imgmsg_to_cv2(image)
        except CvBridgeError as e:
            rospy.logwarn(e.message)
        self.__laserScan = scan
        self.__panoramicImage = image_processing.remapFisheyeToPanorama(self.__image, self.__mapX, self.__mapY)
        self._callback(self)