#!/usr/bin/env python

"""
Module that contains various landmark detectors
"""
 
import rospy
import image_processing
import contour_descriptor_extractors
import contour_landmark_detectors
import numpy as np
import cv2
import descriptors
import landmarks as lm
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
from itertools import count

class Track():
    __ids = count(0)
    __maxCount = 10

    def __init__(self, point):
        self.__id = next(self.__ids)
        self.__points = [point]

    def getId(self):
        """
        Returns:
            (int) - id
        """
        return self.__id
    
    def setId(self, id):
        """
        Args:
            id (int) - id
        """
        self.__id = id

    def getLastPoint(self):
        """
        Return last point
        Returns:
            ([float, float]) - x, y coordinates
        """
        return self.__points[-1]

    def associate(self, imgPoint):
        return np.linalg.norm(self.__points[-1] - imgPoint) < 1

    def update(self, imgPoint):
        """
        Associates and adds a point to the queue
        Args:
            ([float, float]) - x, y coordinates
        """
        self.__points.append(imgPoint)
        if len(self.__points) > self.__maxCount:
            del self.__points[0]

    def drawInImage(self, img):
        """
        Draw track in image
        Args:
            img - image
        """
        cv2.circle(img, (self.__points[-1][0], self.__points[-1][1]), 5, (0, 255, 0), -1)
        cv2.polylines(img, np.int32([self.__points]), False, (0, 0, 255), thickness = 3)
        cv2.putText(img, str(self.__id), (int(self.__points[-1][0]), int(self.__points[-1][1] - 5)), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,255), 2)

class OpticalFlowDetector():

    def __init__(self, cameraInfo, maxLandmarks = 1):
        self.__maxTracks = 10
        self.__maxLandmarks = 10
        self.__detectInterval = 5
        self.__tracks = []
        self.__frameIdx = 0
        self.__lkParams = dict( winSize  = (15, 15), maxLevel = 2, criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))            
        self.__featureParams = dict( maxCorners = 500, qualityLevel = 0.3, minDistance = 7, blockSize = 7 )
        self.__bridge = CvBridge()
        self.__mask = np.zeros(cameraInfo.size, np.uint8)

        center = (int(cameraInfo.center[0]), int(cameraInfo.center[1]))
        radius = int(0.9 * cameraInfo.center[1])
        cv2.circle(self.__mask, center, radius, (255, 0, 0), -1)

        self.__imagePub = rospy.Publisher('/ca_slam/track_image', Image, queue_size=10)

    def compute(self, image, cameraInfo, robot):
        currentImg = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        vis = image.copy()

        if len(self.__tracks) > 0:
            p0 = np.float32([track.getLastPoint() for track in self.__tracks])
            p1, st, err = cv2.calcOpticalFlowPyrLK(self.__previousImg, currentImg, p0, None, **self.__lkParams)
            p0r, st, err = cv2.calcOpticalFlowPyrLK(currentImg, self.__previousImg, p1, None, **self.__lkParams)

            newTracks = []
            for track, point, rPoint in zip(self.__tracks, p1, p0r):
                if track.associate(rPoint) and len(newTracks) < self.__maxLandmarks: 
                    track.update(point)
                    track.drawInImage(vis)
                    newTracks.append(track)

            self.__tracks = newTracks

        landmarks = []
        for track in self.__tracks:
            imgPoint = track.getLastPoint()
            pointInSpace = image_processing.image_processing.cam2world(imgPoint, cameraInfo)
            landmark = lm.FeatureLandmark(pointInSpace, imgPoint, robot)
            landmark.descriptor = descriptors.ImageDescriptor(track.getId())
            landmarks.append(landmark)

        if self.__frameIdx % self.__detectInterval == 0:
            points = cv2.goodFeaturesToTrack(currentImg, mask = self.__mask, **self.__featureParams)
            if points is not None:
                for point in np.float32(points):
                    self.__tracks.append(Track(point[0]))

        self.__frameIdx += 1
        self.__previousImg = currentImg

        self.__imagePub.publish(self.__bridge.cv2_to_imgmsg(vis, 'bgr8'))
        return landmarks