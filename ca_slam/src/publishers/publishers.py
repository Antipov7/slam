#!/usr/bin/env python

"""
Module that contains classes for posting messages for further processing and visualization of data
"""

import rospy
import math
import numpy as np
import cv2 as cv
from abc import ABCMeta, abstractmethod
from contour_analisys import contour_encoding, contour_fragments
from sensor_msgs.msg import LaserScan, PointCloud, ChannelFloat32, Image
from geometry_msgs.msg import Point32, Twist
from nav_msgs.msg import Odometry
from visualization_msgs.msg import Marker
from occupancy_map_pub import OccupancyGridPub
from fisheye_point_cloud_pub import FisheyePointCloudPub
from cv_bridge import CvBridge

class PublisherManager():
    """
    Class that manages publishers
    """
    def __init__(self):
        self._publishers = []

    def add(self, publisher):
        """
        Args:
            publisher (Publisher) - publisher
        """
        self._publishers.append(publisher)

    def remove(self, publisher):
        """
        Args:
            publisher (Publisher) - publisher
        """
        self._publishers.remove(publisher)

    def publish(self, sensorData, landmarksDatabase, robot):
        """
        sensorData (SensorData) - data obtained from sensors. The main thing is that the dataSensor type matches the appropriate publisher
        landmarksDatabase (LandmarkDatabase) - landmarks database
        robot (RobotModel) - model describing robot
        """
        for publisher in self._publishers:
            publisher.publish(sensorData, landmarksDatabase, robot)

class Publisher():
    """
    Abstract class describing publisher
    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def publish(self, sensorData, landmarksDatabase, robot):
        """
        sensorData (SensorData) - data obtained from sensors. The main thing is that the dataSensor type matches the appropriate publisher
        landmarksDatabase (LandmarkDatabase) - landmarks database
        robot (RobotModel) - model describing robot
        """
        pass

class RobotMarkerPublisher(Publisher):
    """
    A class that publishes a robot position
    """

    def __init__(self):
        self.__robotMarkerPub = rospy.Publisher('/robotMarker', Marker, queue_size=1)

    def publish(self, sensorData, landmarksDatabase, robot):
        """
        sensorData (SensorData) - data obtained from sensors. The main thing is that the dataSensor type matches the appropriate publisher
        landmarksDatabase (LandmarkDatabase) - landmarks database
        robot (RobotModel) - model describing robot
        """
        robotPosition = robot.getPosition()
        marker = Marker()
        marker.header.stamp = rospy.Time.now()
        marker.header.frame_id = "/odom"
        marker.type = Marker.ARROW

        marker.pose.position.x = robotPosition[0]
        marker.pose.position.y = robotPosition[1]

        marker.pose.orientation.x = 0.0
        marker.pose.orientation.y = 0.0
        marker.pose.orientation.z = math.sin(robotPosition[2] / 2.0 + math.pi / 4.0)
        marker.pose.orientation.w = math.cos(robotPosition[2] / 2.0 + math.pi / 4.0)

        marker.scale.x = 1.0
        marker.scale.y = 0.2
        marker.scale.z = 0.2

        marker.color.r = 0.0
        marker.color.g = 1.0
        marker.color.b = 0.0
        marker.color.a = 1.0
        
        self.__robotMarkerPub.publish(marker)

class RobotOdometryPublisher(Publisher):
    """
    A class that publishes a odometry of a robot with the covariance
    """

    def __init__(self):
        self.__robotOdomPub = rospy.Publisher('/ca_slam_odom', Odometry, queue_size=1)

    def publish(self, sensorData, landmarksDatabase, robot):
        """
        sensorData (SensorData) - data obtained from sensors. The main thing is that the dataSensor type matches the appropriate publisher
        landmarksDatabase (LandmarkDatabase) - landmarks database
        robot (RobotModel) - model describing robot
        """
        robotPosition = robot.getPosition()
        covariance = robot.getCovarianceMatrix()
        odom = Odometry()
        odom.header.stamp = rospy.Time.now()
        odom.header.frame_id = "/odom"
        odom.child_frame_id = "base_link"
        odom.pose.pose.position.x = robotPosition[0]
        odom.pose.pose.position.y = robotPosition[1]
        odom.pose.pose.position.z = 0
        odom.pose.pose.orientation.z = math.sin(robotPosition[2] / 2.0 + math.pi / 4.0)
        odom.pose.pose.orientation.w = math.cos(robotPosition[2] / 2.0 + math.pi / 4.0)

        odom.pose.covariance[0] = covariance[0, 0]
        odom.pose.covariance[1] = covariance[0, 1]
        odom.pose.covariance[5] = covariance[0, 2]

        odom.pose.covariance[6] = covariance[1, 0]
        odom.pose.covariance[7] = covariance[1, 1]
        odom.pose.covariance[11] = covariance[1, 2]

        odom.pose.covariance[30] = covariance[2, 0]
        odom.pose.covariance[31] = covariance[2, 1]
        odom.pose.covariance[35] = covariance[2, 2]

        self.__robotOdomPub.publish(odom)

class LandmarksPublisher(Publisher):
    """
    A class that publishes all lidar-detected landmarks
    """

    def __init__(self):
        self.__landmarksPub = rospy.Publisher('/landmarks', PointCloud, queue_size=1)

    def publish(self, sensorData, landmarksDatabase, robot):
        """
        sensorData (SensorData) - data obtained from sensors. The main thing is that the dataSensor type matches the appropriate publisher
        landmarksDatabase (LandmarkDatabase) - landmarks database
        robot (RobotModel) - model describing robot
        """
        landmarks = landmarksDatabase.getLandmarks()
        pointCloud = PointCloud()
        pointCloud.header.stamp = rospy.Time.now()
        pointCloud.header.frame_id = "/odom"
        pointCloud.channels.append(ChannelFloat32())
        pointCloud.channels[0].name = 'intensity'

        for (ndx, landmark) in enumerate(landmarks):
            position = landmark.getGlobalPosition()
            if (position.shape[0] == 2):
                pointCloud.points.append(Point32(position[0], position[1], 0))
            else:
                pointCloud.points.append(Point32(position[0], position[1], position[2]))
            pointCloud.channels[0].values.append(landmark.getId())

        self.__landmarksPub.publish(pointCloud)

class ObservedLandmarks(Publisher):
    """
    A class that publishes lidar-observed landmarks
    """

    def __init__(self):
        self.__observedLandmarksPub = rospy.Publisher('/observedLandmarks', PointCloud, queue_size=1)

    def publish(self, sensorData, landmarksDatabase, robot):
        """
        sensorData (SensorData) - data obtained from sensors. The main thing is that the dataSensor type matches the appropriate publisher
        landmarksDatabase (LandmarkDatabase) - landmarks database
        robot (RobotModel) - model describing robot
        """
        landmarks = landmarksDatabase.getObservedLandmarks()
        pointCloud = PointCloud()
        pointCloud.header.stamp = rospy.Time.now()
        pointCloud.header.frame_id = "/odom"
        pointCloud.channels.append(ChannelFloat32())
        pointCloud.channels[0].name = 'intensity'

        for (ndx, landmark) in enumerate(landmarks):
            position = landmark.getGlobalPosition()
            if (position.shape[0] == 2):
                pointCloud.points.append(Point32(position[0], position[1], 0))
            else:
                pointCloud.points.append(Point32(position[0], position[1], position[2]))
            pointCloud.channels[0].values.append(landmark.getId())

        self.__observedLandmarksPub.publish(pointCloud)

class ContourPublisher(Publisher):
    """
    A class that publishes a lidar outline
    """
    def __init__(self):
        self.__pointCloudPub = rospy.Publisher('/contour', PointCloud, queue_size=1)

    def publish(self, sensorData, landmarksDatabase, robot):
        """
        sensorData (SensorData) - data obtained from sensors. The main thing is that the dataSensor type matches the appropriate publisher
        landmarksDatabase (LandmarkDatabase) - landmarks database
        robot (RobotModel) - model describing robot
        """
        robotPosition = robot.getPosition()
        laserScan = sensorData.getLaserScan()
        ranges = np.clip(laserScan.ranges, -1, laserScan.range_max)
        ranges = ranges[ranges > 0.01]

        polarCode = contour_encoding.convertLaserScanToPolarCode(ranges)
        differenceCode = contour_encoding.convertPolarCodeToDifferenceCode(polarCode)
        contourCode = contour_encoding.equivalenceOfContourCode(differenceCode, 0.05)

        pointCloud = contour_encoding.convertContourCodeToMsgPointCloud(contourCode, ranges[0], [robotPosition[0], robotPosition[1], robotPosition[2]])
        pointCloud.header.stamp = rospy.Time.now()
        pointCloud.header.frame_id = "/odom"
        pointCloud.channels.append(ChannelFloat32())
        pointCloud.channels[0].name = 'intensity'

        self.__pointCloudPub.publish(pointCloud)

class ContourFragmentsPublisher(Publisher):
    """
    A class that publishes a contour fragments
    """
    def __init__(self):
        self.__pointCloudPub = rospy.Publisher('/contourFragments', PointCloud, queue_size=1)

    def publish(self, sensorData, landmarksDatabase, robot):
        """
        sensorData (SensorData) - data obtained from sensors. The main thing is that the dataSensor type matches the appropriate publisher
        landmarksDatabase (LandmarkDatabase) - landmarks database
        robot (RobotModel) - model describing robot
        """
        laserScan = sensorData.getLaserScan()
        ranges = np.clip(laserScan.ranges, -1, laserScan.range_max)
        scan = ranges[ranges > 0.01]
        polarCode = contour_encoding.convertLaserScanToPolarCode(scan)
        differenceCode = contour_encoding.convertPolarCodeToDifferenceCode(polarCode)
        fragments = contour_fragments.extractContourFragments(differenceCode, scan[0], [0.0, 0.0], 1)

        pointCloud = PointCloud()
        pointCloud.header.stamp = rospy.Time.now()
        pointCloud.header.frame_id = "/odom"
        pointCloud.channels.append(ChannelFloat32())
        pointCloud.channels[0].name = 'intensity'

        for fragment in fragments:
            for point in fragment.pointCloud:
                pointCloud.points.append(Point32(point[0], point[1], 0))

        self.__pointCloudPub.publish(pointCloud)

class OccupancyMapPublisher(Publisher):
    """
    A class that publishes a occupancy map
    """
    def __init__(self):
        self.__occupancyMapPub = OccupancyGridPub()

    def publish(self, sensorData, landmarksDatabase, robot):
        """
        sensorData (SensorData) - data obtained from sensors. The main thing is that the dataSensor type matches the appropriate publisher
        landmarksDatabase (LandmarkDatabase) - landmarks database
        robot (RobotModel) - model describing robot
        """
        self.__occupancyMapPub.updateOccupancyMap(robot.getPosition(), sensorData.getLaserScan().ranges)

class ImageMatcherPublisher(Publisher):
    """
    """
    def __init__(self, maxKeyPoints = 10):
        self.__maxKeyPoints = maxKeyPoints
        self.__bridge = CvBridge()
        self.__orb = cv.ORB_create(500)
        self.__bf = cv.BFMatcher(cv.NORM_HAMMING, crossCheck=True)
        self.__imagePub = rospy.Publisher('/ca_slam/matches_image', Image, queue_size=10)
        self.__previouseImage = None

    def publish(self, sensorData, landmarksDatabase, robot):
        """
        """
        if self.__previouseImage is None:
            self.__previouseImage = cv.cvtColor(sensorData.getImage(), cv.COLOR_BGR2GRAY)
            return

        image = cv.cvtColor(sensorData.getImage(), cv.COLOR_BGR2GRAY)
        
        kp1 = self.__orb.detect(self.__previouseImage, None)
        kp2 = self.__orb.detect(image, None)

        kp1, des1 = self.__orb.compute(self.__previouseImage, kp1)
        kp2, des2 = self.__orb.compute(image, kp2)

        matches = self.__bf.match(des1, des2)
        matches = sorted(matches, key=lambda x: x.distance)

        matchesCount = min(len(matches), self.__maxKeyPoints)
        matchesImage = cv.drawMatches(self.__previouseImage, kp1, image, kp2, matches[:matchesCount],  None, flags=2)
        self.__previouseImage = image
        self.__imagePub.publish(self.__bridge.cv2_to_imgmsg(matchesImage, 'bgr8'))

class ImagePublisher(Publisher):
    """
    """
    def __init__(self):
        self.__bridge = CvBridge()
        self.__imagePub = rospy.Publisher('/ca_slam/image', Image, queue_size=10)

    def publish(self, sensorData, landmarksDatabase, robot):
        """
        sensorData (SensorData) - data obtained from sensors. The main thing is that the dataSensor type matches the appropriate publisher
        landmarksDatabase (LandmarkDatabase) - landmarks database
        robot (RobotModel) - model describing robot
        """
        img = sensorData.getImage()
        self.__imagePub.publish(self.__bridge.cv2_to_imgmsg(img, 'bgr8'))

class PanoramicImagePublisher(Publisher):
    """
    """
    def __init__(self):
        self.__bridge = CvBridge()
        self.__imagePub = rospy.Publisher('/ca_slam/panorama_image', Image, queue_size=10)

    def publish(self, sensorData, landmarksDatabase, robot):
        """
        sensorData (SensorData) - data obtained from sensors. The main thing is that the dataSensor type matches the appropriate publisher
        landmarksDatabase (LandmarkDatabase) - landmarks database
        robot (RobotModel) - model describing robot
        """
        img = sensorData.getPanoramicImage()
        self.__imagePub.publish(self.__bridge.cv2_to_imgmsg(img, 'bgr8'))

class FisheyePointCloudPublisher(Publisher):
    """
    """
    def __init__(self):
        self.__fisheyePointCloudPub = FisheyePointCloudPub()

    def publish(self, sensorData, LandmarksPublisher, robot):
        """
        """
        self.__fisheyePointCloudPub.publish(sensorData.getPanoramicImage(), np.eye(3, 3), np.eye(3, 3))