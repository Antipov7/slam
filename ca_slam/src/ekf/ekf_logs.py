#!/usr/bin/env python

"""
Module contains arious logging classes
"""

import numpy as np
from toolbox import sio
from abc import ABCMeta, abstractmethod

class EkfLog():
    """
    Abstract class of ekf log
    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def info(self, map, landmarksDatabase, deltaTime):
        """
        Displays map information
        Args:
            map (EkfMap) - ekf map
            landmarksDatabase (LandmarksDatabase) - landmarks database
            deltaTime (float) - delta time
        """
        pass

class EmptyEkfLog():
    """
    Class that does nothing
    """

    def info(self, map, landmarksDatabase, deltaTime):
        """
        Displays map information
        Args:
            map (EkfMap) - ekf map
            landmarksDatabase (LandmarksDatabase) - landmarks database
            deltaTime (float) - delta time
        """
        pass

class EkfJLog(EkfLog):
    """
    Class that saves map state to json file
    """

    def __init__(self, mapName, elapsedTime):
        """
        Args:
            mapName (string) - map name
        """
        self.__mapName = mapName
        self.__fileNumber = 0
        self.__path = sio.getPathToFilesFolder() + mapName
        sio.createFolder(self.__path)
        self.__time = 0.0
        self.__totalTime = 0.0
        self.__elapsedTime = elapsedTime

    def info(self, map, landmarksDatabase, deltaTime):
        """
        Displays map information
        Args:
            map (EkfMap) - ekf map
            landmarksDatabase (LandmarksDatabase) - landmarks database
            deltaTime (float) - delta time
        """
        self.__totalTime += deltaTime
        self.__time += deltaTime
        if self.__time < self.__elapsedTime:
            return

        data = {
            "name" : self.__mapName,
            "time" : self.__totalTime,
            "step" : self.__fileNumber,
            "landmarksType" : self.__getLandmarksType(landmarksDatabase),
            "landmarksBlock" : self.__getLandmarksBlock(map, landmarksDatabase),
            "observedLandmarks" :  self.__getObservedLandmarks(landmarksDatabase),
            "state" : map.state.tolist(),
            "covariance" : map.covariance.tolist()
            }
        fileName = self.__path + "/map_{:05d}.json".format(self.__fileNumber)
        sio.printToJsonFile(data, fileName)
        self.__fileNumber += 1
        self.__time = 0

    def __getLandmarksType(self, landmarksDatabase):
        """
        Returns an array of landmark types
        Args:
            landmarksDatabase (LandmarksDatabase) - landmarks database
        Returns:
            ([string]) - array of landmarks types
        """
        landmarks = landmarksDatabase.getLandmarks()
        landmarksType = ["" for x in range(len(landmarks) + 1)]
        landmarksType[0] = "robot"
        for landmark in landmarks:
            landmarksType[landmark.getId()] = landmark.getType()
        return landmarksType

    def __getLandmarksBlock(self, map, landmarksDatabase):
        """
        Returns an array of positions and sizes of landmark blocks
        Args:
            map (EkfMap) - ekf map
            landmarksDatabase (LandmarksDatabase) - landmarks database
        Returns:
            ([[float, float]]) - array of positions and sizes of landmark blocks
        """
        landmarks = landmarksDatabase.getLandmarks()
        landmarksBlock = [[0, 0] for x in range(len(landmarks) + 1)]
        landmarksBlock[0] = [0, 3]
        for landmark in landmarks:
            landmarksBlock[landmark.getId()] = map.getPositionMatrixBlockOfSystemObject(landmark.getId())
        return landmarksBlock

    def __getObservedLandmarks(self, landmarksDatabase):
        """
        Returns ids of observed landmarks
        Args:
            landmarksDatabase (LandmarksDatabase) - landmarks database
        Returns:
            ([int]) - ids of observed landmarks
        """
        landmarks = landmarksDatabase.getObservedLandmarks()
        ids = [x.getId() for x in landmarks]
        return ids

class EkfPoseLog(EkfLog):
    """
    Class that saves mobile platform state to csv file
    """

    def __init__(self, mapName):
        """
        Args:
            mapName (string) - map name
        """
        sio.createFolder(sio.getPathToFilesFolder() + mapName)
        self.__csvLog = sio.CSVLog(mapName + "/poses.csv")

    def info(self, map, landmarksDatabase, deltaTime):
        """
        Displays map information
        Args:
            map (EkfMap) - ekf map
            landmarksDatabase (LandmarksDatabase) - landmarks database
            deltaTime (float) - delta time
        """
        self.__csvLog.write([map.state[0], map.state[1], map.state[2]])