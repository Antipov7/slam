#!/usr/bin/env python

"""
Module that contains basic functionality for working with files
"""

import os
import csv
import math
import json
import rospy
import rospkg
import numpy as np

def getPathToFilesFolder():
    """
    Get the path to the files folder
    Returns:
        (string) - path ti the files folder
    """
    rospack = rospkg.RosPack()
    return rospack.get_path('ca_slam') + "/files/"

def getFullName(fileName):
    """
    Returns the full name of the file
    Args:
        fileName (string) - file name
    Returns:
        (string) - full name of the file
    """
    return  getPathToFilesFolder() + fileName

def printToJsonFile(data, fileName, mode = 'w'):
    """
    Save data to json file
    Args:
        data - json data
        fileName (string) - file name
        mode (string) - file read mode
    """
    with open(fileName, mode) as file:
        json.dump(data, file)
    
def createFolder(directory):
    """
    Checks and creates a folders
    Args:
        directory (string) - path
    """
    if not os.path.exists(directory):
        os.makedirs(directory)

class CSVLog():
    """
    The class that writes to the csv file
    """

    def __init__(self, fileName, mode = 'w+'):
        """
        Args:
            fileName (string) - file name
            mode (string) - file read mode
        """
        self.__csvFile = open(getPathToFilesFolder() + fileName, mode='w+')
        self.__csvWriter = csv.writer(self.__csvFile, delimiter=',', quotechar='"', quoting = csv.QUOTE_MINIMAL)

    def write(self, line):
        """
        Writes one line to a file
        Args:
            line (array(float)) - line
        """
        self.__csvWriter.writerow(line)

    def __del__(self):
        self.__csvFile.close()