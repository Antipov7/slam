#!/usr/bin/env python

import unittest
import contour_fragments
import numpy as np

class TestContourEncoding(unittest.TestCase):
        
    def test_extractContourFragments(self):
        contour = [2 + 2j, 1, 1, 1 + 1j, 1 - 1j, 3, -1j, -1j, -1j, -3 - 2j, -3 + 1j, -1, -1 + 1j, -1 + 1j]

        
        contourFragments = [contour_fragments.ContourFragment([1, 1, 1 + 1j, 1 - 1j], np.array([2.0, 2.0])),
                            contour_fragments.ContourFragment([-1j, -1j, -1j], np.array([9.0, 2.0])),
                            contour_fragments.ContourFragment([-1, -1 + 1j, -1 + 1j], np.array([3.0, -2.0]))]
        
        result = contour_fragments.extractContourFragments(contour, 0, [0, 0], 1.5)
        self.assertListEqual(result, contourFragments)