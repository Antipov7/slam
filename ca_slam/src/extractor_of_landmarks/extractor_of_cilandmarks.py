#!/usr/bin/env python

"""
Module that extracts landmarks using a contour analysis and fisheye camera
"""

import rospy
import image_processing
import contour_descriptor_extractors
import contour_landmark_detectors
import numpy as np
import cv2 as cv
import descriptors
import landmarks as lm
from image_landmark_detectors import OpticalFlowDetector 
from extractor_of_landmarks import ExtractorOfLandmarks
from contour_analisys import contour_encoding, contour_fragments
from sensor_data import LaserScanAndImageData

class CIExtractorOfLandmarks(ExtractorOfLandmarks):
    """
    Extract landmarks using contour analisys and opencv
    """
    def __init__(self, cameraInfo):
        """
        Initialization using getting parameters from rospy
        """
        self.__cornerDetector = contour_landmark_detectors.initCornerDetectorUsingRospy()
        self.__cornerDescriptorExctractor = contour_descriptor_extractors.initCornerDescriptorExtractorUsingRospy()
        self.__straightLineDetector = contour_landmark_detectors.initStraightLineDetectorUsingRospy()
        self.__straightLineDescriptorExctractor = contour_descriptor_extractors.initStraightLineDescriptorExtractorUsingRospy()
        self.__imageFeatureDetector = OpticalFlowDetector(cameraInfo)

    def extractLandmarks(self, laserScanAndImageData, robot):
        """
        Extract landmarks
        Args:
            laserScanAndImageData (LaserScanAndImageData) - laser scan and image data
            robot (RobotModel) - model of robot
        Returns:
            list(landmark) - landmarks
        """
        #if not isinstance(laserScanAndImageData, type(LaserScanAndImageData)):
            #raise ValueError("laserScanAndImageData must be type LaserScanAndImageData. Check the configuration slam algorithm")
        
        landmarks = []
        robotPosition = robot.getPosition()
        laserScan = laserScanAndImageData.getLaserScan()
        image = laserScanAndImageData.getImage()
        scan = np.clip(laserScan.ranges, laserScan.range_min, laserScan.range_max)
        polarCode = contour_encoding.convertLaserScanToPolarCode(scan)
        differenceCode = contour_encoding.convertPolarCodeToDifferenceCode(polarCode)
        fragments = contour_fragments.extractContourFragments(differenceCode, scan[0], [0.0, 0.0], 0.5)
        cornerLandmarks = self._cornerLandmarkDetect(fragments, robotPosition)
        imageLandmarks = self.__imageFeatureDetect(laserScanAndImageData.getImage(), robot, laserScanAndImageData.getCameraInfo())
        landmarks = imageLandmarks + cornerLandmarks
        return landmarks

    def _cornerLandmarkDetect(self, contourFragments, robotPosition):
        """
        Args:
            contourFragments (list(ContourFragment)) - contour fragments
            robotPosition ([float, float, float]) - x, y, theta position of robot 
        Returns:
            (list(CornerLandmark)) - landmarks
        """
        cornerLandmarks, fragments, ndxs = self.__cornerDetector.detect(contourFragments, robotPosition)
        if (cornerLandmarks is None):
            return None 
        for i, landmark in enumerate(cornerLandmarks):
            landmark.descriptor = self.__cornerDescriptorExctractor.compute(fragments[i], ndxs[i], robotPosition)
        return cornerLandmarks

    def _straightLineDetect(self, contourFragments, image, robotPosition):
        """
        Args:
            contourFragments (list(ContourFragment)) - contour fragments
            image (cv_image) - color image with rgb8
            robotPosition ([float, float, float]) - x, y, theta position of robot 
        Returns:
            (list(CornerLandmark)) - landmarks
        """
        lineLandmarks, fragments, lines = self.__straightLineDetector.detect(contourFragments, robotPosition)
        if (lineLandmarks is None):
            return None
        for i, landmark in enumerate(lineLandmarks):
            landmark.descriptor = self.__straightLineDescriptorExctractor.compute(fragments[i].pointCloud, lines[i], robotPosition)
        return lineLandmarks

    def __imageFeatureDetect(self, image, robot, cameraInfo):
        """
        Args:
            image (cv_image) - color image with rgb8
            robot (RobotModel) - model of robot
            cameraInfo (OcamCalib) - camera info
        Returns:
            image landmraks
        """      
        return self.__imageFeatureDetector.compute(image, cameraInfo, robot)