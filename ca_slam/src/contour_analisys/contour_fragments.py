#!/usr/bin/env python

"""
Module for processing contour fragments
"""

import rospy
import numpy as np
import contour_encoding

class ContourFragment():
    """
    Class describing a fragment of a contour
    """
    def __init__(self, contourCode, pointCloud):
        """
        Args:
            contourCode (array(complex)) - difference code
            pointCloud (array([float, float])) - point describing the origin of fragment of a contour
        """
        self.contour = contourCode
        self.pointCloud = pointCloud

    def __eq__(self, other):
        return np.all(self.contour == other.contour) and np.all(self.firstPoint == other.firstPoint)

def extractContourFragments(differenceCode, firstScanValue, contourPose, threshold = 1):
    """
    Extracts fragments of a contour
    Args:
        differenceCode (array(complex)) - difference contour code
        firstScanValue (float) - first value of the lidar scan
        contourPose ([float, float]) - center of contour (position of lidar)
        threshold (float) - threshold, more of which the contour is divided into fragments
    Returns:
        list(ContourFragment) - fragments of a contour
    """
    contourFragments = []
    pointCloud = contour_encoding.convertContourCodeToPointCloud(differenceCode, [contourPose[0] + firstScanValue, contourPose[1]])
    splitNdxs = np.where(abs(differenceCode) > threshold)[0]
    dist = np.array([np.linalg.norm(x - contourPose) for x in pointCloud])
    splitNdxs = np.concatenate((np.where(dist > 4.5)[0], splitNdxs), axis=0)
    if (np.linalg.norm(pointCloud[0] - pointCloud[-1]) > threshold):
        splitNdxs = np.append(splitNdxs, 0)

    splitNdxs = np.unique(splitNdxs)
    if (len(splitNdxs) == 0):
        contourFragments.append(__getEquivalenceContourFragment(differenceCode, pointCloud[0]))
        return contourFragments

    differenceCode = np.roll(differenceCode, -splitNdxs[0])
    pointCloud = np.roll(np.array(pointCloud), -splitNdxs[0], axis=0)

    splitNdxs -= splitNdxs[0]
    splitNdxs = np.delete(splitNdxs, 0)
    contourFragmentsCode = np.split(differenceCode, splitNdxs)
    pointCloudFragments = np.split(pointCloud, splitNdxs, axis=0)
    for (ndx, fragment) in enumerate(contourFragmentsCode):
        if (len(fragment) > 1):
            fragment = np.delete(fragment, 0)
            contourFragments.append(__getEquivalenceContourFragment(fragment, pointCloudFragments[ndx][1]))

    return contourFragments

def __getEquivalenceContourFragment(differenceCode, firstPoint, lengthOfSegments = 0.05):
    """
    Return a ContourFragment by converting the contour to the equivalent code
    Args:
        differenceCode (array(complex)) - difference contour code
        firstPoint (array([float, float])) - The first point of contour
        lengthOfSegments (float) - Approximate length of segments
    Returns:
        ContourFragment - contour fragment
    """
    equivalenceCode = contour_encoding.equivalenceOfContourCode(differenceCode, lengthOfSegments)
    pointCloud = contour_encoding.convertContourCodeToPointCloud(equivalenceCode, firstPoint)
    return ContourFragment(equivalenceCode, pointCloud)