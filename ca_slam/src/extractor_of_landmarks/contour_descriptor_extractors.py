#!/usr/bin/env python

"""
Module that contains descriptor extractors
"""

import rospy
import descriptors
import numpy as np
import math
from toolbox import smath
from abc import ABCMeta, abstractmethod

def initCornerDescriptorExtractorUsingRospy():
    """
    Corner descriptor initialization
    """
    descriptorExtractor = PlaceCornerDescriptorExtractor()
    return descriptorExtractor

def initStraightLineDescriptorExtractorUsingRospy():
    """
    Straight line descriptor initialization
    """
    descriptorExtractor = PolarParametersStraightLineDescriptor()
    return descriptorExtractor

class CornerDescriptorExtractor():
    """
    Abstract corner descriptor extractor class 
    """
    __metaclass__  = ABCMeta

    @abstractmethod
    def compute(self, contourFragment, ndx, robotPosition):
        """
        Args:
            contourFragment (ContourFragment) - contour fragment
            ndx (int) - index position of the landmark on the contour fragment
            robotPosition ([float, float, float]) - x, y, theta position of robot 
        """
        pass

class StraightLineDescriptorExtractor():
    """
    Abstract straight line descriptor extractor class
    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def compute(self, pointCloud, landmarkPolarParameters):
        """
        Args:
            pointCloud (array([float, float])) - point cloud
            landmarkPolarParameters ([float, float]) - r, alpha parameters
        """
        pass

class PlaceCornerDescriptorExtractor(CornerDescriptorExtractor):
    """
    Extractor PlaceCornerDescription
    """

    def compute(self, contourFragment, ndx, robotPosition):
        """
        Compute place corner descriptor
        Args:
            contourFragment (ContourFragment) - contour fragment
            ndx (int) - index position of the landmark on the contour fragment
            robotPosition ([float, float, float]) - x, y, theta position of robot 
        """
        return descriptors.PlaceCornerDescription(contourFragment, ndx, robotPosition)

class PolarParametersStraightLineDescriptor(StraightLineDescriptorExtractor):
    """
    Extractor PolarParametersStraightLineDescriptor
    """

    def compute(self, pointCloud, polarLineParameters, robotPosition):
        """
        Args:
            pointCloud (array([float, float])) - point cloud
            polarLineParameters ([float, float]) - r, alpha parameters
            robotPosition ([float, float, float]) - x, y, theta position of robot 
        """
        center = smath.rotate2D(robotPosition, -robotPosition[2])
        smath.straightLinePolarTranslation(polarLineParameters[0], polarLineParameters[1], center[0], center[1], robotPosition[2])
        return descriptors.PolarParametersStraightLineDescriptor(np.array([abs(c), alpha]))

class CornerPanoramicImageDescriptorExtractor():
    """
    Excrator image descriptor from panoramic image
    """
    
    def compute(self, image, landmarkPosition, robotPosition):
        """
        Compute corner image descriptor
        Args:
            image (cv_image) - panoramic image with rgb8
            landmarkPosition ([float, float]) - local x and y coordinates are relative to the robot
            robotPosition ([float, float, float]) - x, y, theta position of robot 
        Returns:
            (descriptor) - descriptor
        """
        pass