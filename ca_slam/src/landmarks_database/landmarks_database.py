#!/usr/bin/env python

"""
Module containing a class describing a database of landmarks
"""

from abc import ABCMeta, abstractmethod

class LandmarksDatabase:
    """
    """
    __metaclass__  = ABCMeta
    
    def __init__(self):
        self._landmarks = []
        self._lastObservedLandmarks = []
        self._newLandmarks = []

    def landmarksAssociation(self, landmarks):
        """
        Landmarks association
        """
        self._lastObservedLandmarks = []
        self._newLandmarks = []

    def getObservedLandmarks(self):
        """
        Returns last observed landmarks
        Returns:
            list(Landmark) - landmarks
        """
        return self._lastObservedLandmarks

    def getLandmarks(self):
        """
        Returns all landmarks
        Returns:
            list(Landmark) - landmarks
        """
        return self._landmarks

    def getNewLandmarks(self):
        """
        Returns last added landmarks
        Returns:
            list(Landmark) - landmarks
        """
        return self._newLandmarks

    def _addLandmark(self, landmark):
        """
        Adds landmark to base
        Args:
            landmark (Landmark) - landmark
        """
        landmark.setId(len(self._landmarks) + 1)
        self._newLandmarks.append(landmark)
        self._landmarks.append(landmark)