#!/usr/bin/env python

"""
Module contains algorithms for dividing a global map into local maps
"""

import rospy
import numpy as np
from ekf_map import EkfMap
from abc import ABCMeta, abstractmethod

class LocalMappingAlgorithm():
    """
    Abstract class of local mapping algorithm
    """
    __metaclass__ = ABCMeta

    def buildLocalMap(self, globalMap, robot, landmarksDatabase):
        """
        Builds a local map
        Args:
            globalMap (EkfMap) - global map
            robot (RobotModel) - robot
            landmarksDatabase (LandmarkDatabase) - landmarks database
        Returns:
            (EkfMap) - localMap
            (list(Landmarks)) - landmarks that refer to the local map
            (list(Landmarks)) - observed landmarks that refer to the local map
        """
        landmarks, observedLandmarks = self._selectLandmarksForLocalMap(robot, landmarksDatabase)
        localMap = EkfMap(robot)
        for landmark in landmarks:
            localMap.addSystemObjectToMap(landmark)

        ids = [landmark.getId() for landmark in landmarks]
        ids.append(robot.getId())
        for firstId in ids:
            for secondId in ids:
                if (firstId == secondId):
                    continue
                localMap.setCovarianceBetweenSystemObjects(globalMap.getCovarianceBetweenSystemObjects(firstId, secondId), firstId, secondId)
        return localMap, landmarks, observedLandmarks

    def mergeLocalMapToGlobalMap(self, localMap, globalMap):
        """
        Merge a local map with a global map. Changes state and covariance in global map
        Args:
            globalMap (EkfMap) - global map
            localMap (EkfMap) - local map
        """
        globalMap.mergeMap(localMap)

    @abstractmethod
    def _selectLandmarksForLocalMap(robot, landmarksDatabase):
        """
        Choose landmarks that will be used to build a local map
        Args:
            robot (RobotModel) - model describing robot
            landmarksDatabase (LandmarksDatabase) - landmarks database
        Returns:
            (list(Landmarks)) - landmarks that refer to the local map
            (list(Landmarks)) - observed landmarks that refer to the local map
        """
        pass

class GlobalMapping(LocalMappingAlgorithm):
    """
    Class is a dummy that returns a global map and does nothing more
    """

    def buildLocalMap(self, globalMap, robot, landmarksDatabase):
        """
        Builds a local map
        Args:
            globalMap (EkfMap) - global map
            robot (RobotModel) - robot
            landmarksDatabase (LandmarkDatabase) - landmarks database
        Returns:
            (EkfMap) - localMap
            (list(Landmarks)) - landmarks that refer to the local map
            (list(Landmarks)) - observed landmarks that refer to the local map
        """
        return globalMap, landmarksDatabase.getLandmarks(), landmarksDatabase.getObservedLandmarks()

    def mergeLocalMapToGlobalMap(self, localMap, globalMap):
        """
        Merge a local map with a global map. Changes state and covariance in global map
        Args:
            globalMap (EkfMap) - global map
            localMap (EkfMap) - local map
        """
        pass

    def _selectLandmarksForLocalMap(robot, landmarksDatabase):
        """
        Choose landmarks that will be used to build a local map
        Args:
            robot (RobotModel) - model describing robot
            landmarksDatabase (LandmarksDatabase) - landmarks database
        Returns:
            (list(Landmarks)) - landmarks that refer to the local map
            (list(Landmarks)) - observed landmarks that refer to the local map
        """
        pass

class AdaptiveObservationRange(LocalMappingAlgorithm):
    """
    Local mapping algorithm. Adaptive observation range
    """

    def __init__(self, minRadius = 1.0, maxRadius = 40.0, minLandmarksCount = 10, maxLandmarksCount = 20):
        """
        Args:
            minRadius (int) - minimum radius of observation area
            maxRadius (int) - maximum radius of observation area
            minLandmarksCount (int) - minimum number of landmarks that will be used for the local map
            maxLandmarksCount (int) - maximum number of landmarks that will be used for the local map
        """
        self.__step = (maxRadius - minRadius) / 40.0
        self.__minRadius = minRadius
        self.__maxRadius = maxRadius
        self.__currentRadius = maxRadius
        self.__minLandmarksCount = minLandmarksCount
        self.__maxLandmarksCount = maxLandmarksCount

    def _selectLandmarksForLocalMap(self, robot, landmarkDatabase):
        """
        Choose landmarks that will be used to build a local map
        Args:
            robot (RobotModel) - model describing robot
            landmarksDatabase (LandmarksDatabase) - landmarks database
        Returns:
            list(Landmark) - landmarks
            list(Landmark) - observedLandmark
        """
        landmarks = []
        observedLandmarks = landmarkDatabase.getLandmarks()
        sortedLandmarks = sorted(observedLandmarks, key=lambda item: np.linalg.norm(item.getCurrentMeasurement()))
        for landmark in sortedLandmarks:
            if (np.linalg.norm(landmark.getCurrentMeasurement()) <= self.__currentRadius):
                landmarks.append(landmark)

        if (len(landmarks) > self.__maxLandmarksCount):
            self.__currentRadius -= self.__step
            self.__currentRadius = np.clip(self.__currentRadius, self.__minRadius, self.__maxRadius)
        elif (len(landmarks) < self.__minLandmarksCount):
            self.__currentRadius += self.__step
            self.__currentRadius = np.clip(self.__currentRadius, self.__minRadius, self.__maxRadius)
        
        observedLandmarks = landmarkDatabase.getObservedLandmarks()
        for landmark in observedLandmarks:
            if landmark not in landmarks:
                landmarks.append(landmark)

        return landmarks, observedLandmarks

class DivideAndConquer(LocalMappingAlgorithm):
    """
    Local mapping algorithm. Divide and Conquer SLAM
    """

    def __init__(self, maxCountLandmarks = 44):
        """
        Args:
            maxCountLandmarks (int) - maximum number of landmarks used for a local map
        """
        self.__maxCountLandmarks = maxCountLandmarks
        self.__localMapLandmarks = []

    def _selectLandmarksForLocalMap(self, robot, landmarkDatabase):
        """
        Choose landmarks that will be used to build a local map
        Args:
            robot (RobotModel) - model describing robot
            landmarksDatabase (LandmarksDatabase) - landmarks database
        Returns:
            list(Landmark) - landmarks
            list(Landmark) - observedLandmark
        """
        observedLandmarks = landmarkDatabase.getObservedLandmarks()
        if (len(self.__localMapLandmarks) > self.__maxCountLandmarks):
            self.__localMapLandmarks = []

        for landmark in observedLandmarks:
            if landmark not in self.__localMapLandmarks:
                self.__localMapLandmarks.append(landmark)

        return self.__localMapLandmarks, observedLandmarks